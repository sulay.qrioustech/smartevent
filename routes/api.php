<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// //EventUser Registration
// Route::post('/eventregistration','Api\EventRegistrationController@registration');

// //BusinessUser Registration
// Route::post('/businessregistration','Api\BusinessRegistrationController@registration');


//Users Registration
Route::post('/userregistration','Api\UserController@registration');

//Users Verification Controller
Route::post('/verification','Api\VerificationController@verification');

//Contact Us
Route::post('/contact','Api\ContactController@insert');

//Login
Route::post ('/login','Api\LoginController@login');

//About Us
Route::get('/about','Api\AboutController@about');

//Event Create
Route::post('/event/create','Api\CreateEventController@event');

//Event Create
Route::post('/create/event','Api\CreateEventController@createevent');

//Image upload
Route::post('/image/upload','Api\UploadImageController@upload');

//Business Create
Route::post('/business/create','Api\CreateBusinessController@business');

//business_id
Route::post('/businessid','Api\CreateBusinessController@getBusinessId');

//Qrcode
Route::get('/qrcode','Api\CreateBusinessController@qrcode');

//Edit Business
Route::post('/business/edit','Api\CreateBusinessController@edit');

//Business Details
Route::post('/business/detail','Api\CreateBusinessController@getbusiness');

//EventDetail Api
Route::post('/Event/detail','Api\EventDetailController@getEventDetail');

//EventDetail Api Clone
Route::post('/Event/detailClone','Api\EventDetailController@getEventDetailClone');

//BusinessDetail Api
Route::post('/Business/details','Api\BusinessDetailController@business');

//BusinessList Api
Route::post('/Business/list','Api\BusinessListController@getBusinessList');

//Single Business List Api
Route::post('/Business/list/single','Api\BusinessListController@singleBusinessDetail');

//eventregistered
Route::post('/Business/event/register','Api\BusinessListController@selectEvent');

//selected event list
Route::post('select/events/list','Api\BusinessListController@selectEventList');

//upcommingEvent Api
Route::get('/upcommingEvent','Api\EventListController@upcommingEvent');

//pastEvent Api
Route::get('/pastEvent','Api\EventListController@pastEvent');

//profile Api
Route::post('/profile','Api\ProfileController@profile');

//EditProfile Api
Route::post('/edit/profile','Api\ProfileController@editprofile');

//favourite business Api
Route::post('/favourite','Api\BusinessListController@favouriteBusiness');

//getFavouriteBusiness api
Route::post('/favourite/list','Api\BusinessListController@getfavouriteBusineess');

//delete favourites
Route::post('/favourite/delete','Api\BusinessListController@deletefavourites');

//forget Password
Route::post('/forgetpassword','Api\ForgetpasswordController@forgetpassword');

//serach event
Route::get('/search/event','Api\CreateEventController@searchEventList');

//event share
Route::get('/shareEvent','Api\CreateEventController@shareEvent');

//Business share
Route::get('/shareBusiness','Api\CreateBusinessController@shareBusiness');

//website_login
Route::post('/website/login','Api\WebsiteLoginController@login');

 //website Business Create
 Route::post('/create/business','Api\CreateBusinessController@createbusiness');

//chat
 Route::post('/add/chat','Api\ChatController@chat');

//chat
Route::post('/chat','Api\ChatController@getchat');

//get chat user list
Route::post('/chat_user_list','Api\ChatController@get_chat_user');

//logout 
Route::post('/logout', 'Api\UserController@logout');