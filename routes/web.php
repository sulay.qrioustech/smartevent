<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts/app');
// });

// Route::get('/index',function()
// {
//     return view('event/index');
// });


Route::middleware('auth')->group(function(){

    //event_graph
    Route::get('/event_chart', 'DashboardController@event_chart')->name('event_chart');

    //Business_chart
    Route::get('/business_chart', 'DashboardController@business_chart')->name('business_chart');

    //User_chart
    Route::get('/user_chart', 'DashboardController@user_chart')->name('user_chart');

    //DashBoard
    Route::get('/dashboard','DashboardController@display')->name('dashboard');

    //UserRegistration show
    Route::get('/UserRegistration/show','UserController@display')->name('userreg-show');

    //UserRegistration Delete
    Route::get('/UserRegistration/delete/{id}','UserController@delete')->name('userreg-delete');

    //User Favourite
    Route::get('/User/favourite/business/{id}','UserController@favourite_business')->name('fav-business');

    //Contact Registration
    Route::get('contact','ContactController@display')->name('contact');

    //Delete Contact
    Route::get('Contact/delete/{id}','ContactController@delete')->name('contact-delete');

    //About Form 
    Route::get('about/add','AboutController@add')->name('about-add');

    //About View
    Route::get('/about','AboutController@show')->name('about-show');

    //About Insert
    Route::post('/about/insert','AboutController@insert')->name('about-insert');

    //About Edit
    Route::get('/about/edit','AboutController@edit')->name('about-edit');

    //About Update
    Route::post('/about/update/{id}','AboutController@update')->name('about-update');

    //Event 
    Route::get('/event','EventController@view')->name('event');

    //approve event
    Route::get('/event/approve/{id}','EventController@approve')->name('event-approve');

    // disapprove event
    Route::get('/event/disapprove/{id}','EventController@disapprove')->name('event-disapprove');

    //event - delete
    Route::get('/event/delete/{id}','EventController@delete')->name('event-delete');

    //Event show Business
    Route::get('/event/business/view/{id}','EventController@showBusiness')->name('event-business-view');

    //business delete
    Route::get('business/delete/{id}','BusinessController@delete')->name('business-delete');

    //view Business Detail
    Route::get('/business','BusinessController@view')->name('business');

    //aprrove business
    Route::get('/business/approve/{id}','BusinessController@approve')->name('business-approve');

    //disapprove business
    Route::get('/business/disapprove/{id}','BusinessController@disapprove')->name('business-disapprove');

    //view Business Organizarr Detail
    Route::get('/business/organizar/detail/{id}','BusinessController@businessOrganizarDetail')->name('business-organizar-detail');

    //favourite User
    Route::get('business/favourite/user/{id}','BusinessController@favourite')->name('favourite-user');

    //All user mail
    Route::get('/email','SendEmailController@view')->name('email');

    //email all user
    Route::post('/email/send','SendEmailController@sendAllUserMail')->name('useremail');

    //user mail
    Route::get('/usermail','SendEmailController@user_show')->name('usermail');

    //event user mail
    Route::get('/eventmail','SendEmailController@event_show')->name('eventmail');

    //business mail
    Route::get('/businessmail','SendEmailController@business_show')->name('businessmail');

    Route::get('/businessapprove/{event_id}/{business_id}','EventController@businessApprove')->name('event-business-approve');
});

//login view
Route::get('/index','LoginController@display')->name('login');

//login 
Route::post('/loginm','LoginController@login')->name('signin');

Route::get('/home', 'HomeController@index')->name('home');



   
    //Users Registration
    Route::post('/userregistration','Api\UserController@registration');
    
    //Users Verification Controller
    Route::post('/verification','Api\VerificationController@verification');
    
    //Contact Us
    Route::post('/contact','Api\ContactController@insert');
    
    //Login
    Route::post ('/login','Api\LoginController@login');
    
    //About Us
    Route::get('/about','Api\AboutController@about');
    
    //Event Create
    Route::post('/event/create','Api\CreateEventController@event');
    
    //Event Create
    Route::post('/create/event','Api\CreateEventController@createevent');
    
    //Image upload
    Route::post('/image/upload','Api\UploadImageController@upload');
    
    //Business Create
    Route::post('/business/create','Api\CreateBusinessController@business');

    //website Business Create
    Route::post('/create/business','Api\CreateBusinessController@createbusiness');
    
    //business_id
    Route::post('/businessid','Api\CreateBusinessController@getBusinessId');
    
    //Qrcode
    Route::get('/qrcode','Api\CreateBusinessController@qrcode');
    
    //Edit Business
    Route::post('/business/edit','Api\CreateBusinessController@edit');
    
    //Business Details
    Route::post('/business/detail','Api\CreateBusinessController@getbusiness');
    
    //EventDetail Api
    Route::post('/Event/detail','Api\EventDetailController@getEventDetail');
    
    //EventDetail Api Clone
    Route::post('/Event/detailClone','Api\EventDetailController@getEventDetailClone');
    
    //BusinessDetail Api
    Route::post('/Business/details','Api\BusinessDetailController@business');
    
    //BusinessList Api
    Route::post('/Business/list','Api\BusinessListController@getBusinessList');
    
    //Single Business List Api
    Route::post('/Business/list/single','Api\BusinessListController@singleBusinessDetail');
    
    //eventregistered
    Route::post('/Business/event/register','Api\BusinessListController@selectEvent');
    
    //selected event list
    Route::post('select/events/list','Api\BusinessListController@selectEventList');
    
    //upcommingEvent Api
    Route::get('/upcommingEvent','Api\EventListController@upcommingEvent');
    
    //pastEvent Api
    Route::get('/pastEvent','Api\EventListController@pastEvent');
    
    //profile Api
    Route::post('/profile','Api\ProfileController@profile');
    
    //EditProfile Api
    Route::post('/edit/profile','Api\ProfileController@editprofile');
    
    //favourite business Api
    Route::post('/favourite','Api\BusinessListController@favouriteBusiness');
    
    //getFavouriteBusiness api
    Route::post('/favourite/list','Api\BusinessListController@getfavouriteBusineess');
    
    //delete favourites
    Route::post('/favourite/delete','Api\BusinessListController@deletefavourites');
    
    //forget Password
    Route::post('/forgetpassword','Api\ForgetpasswordController@forgetpassword');
    
    //serach event
    Route::get('/search/event','Api\CreateEventController@searchEventList');
    
    //event share
    Route::get('/shareEvent','Api\CreateEventController@shareEvent');
    
    //Business share
    Route::get('/shareBusiness','Api\CreateBusinessController@shareBusiness');
    
    //website_login
    Route::post('/website/login','Api\WebsiteLoginController@login');

    //website logout
    Route::get('/website/logout','Api\WebsiteLoginController@logout')->name('website-logout');

// ............website route.........//
Route::get('/','websiteController@index')->name('website-home');

Route::get('/contactus','websiteController@contact')->name('website-contact'); 

Route::get('/businesslist','websiteController@business')->name('Business-List');

Route::get('/event/list/{id}','websiteController@business')->name('event-list');

Route::get('/single/event/{id}','websiteController@singleevent')->name('single-event');

Route::get('/business/{id}','websiteController@singlebusiness')->name('single-business');

Route::get('/myevents/{id}','websiteController@events')->name('myevents');

Route::get('/event/select/{id}','websiteController@selectEvent')->name('event-select');

Route::get('/favourite/business','websiteController@favourite')->name('favourite');

Route::get('/favourite/business/list','websiteController@favouriteBusiness')->name('favourite-business');