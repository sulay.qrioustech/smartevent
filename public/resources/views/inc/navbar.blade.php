<header class="site-header">
    <div class="container-fluid">
        <a href="#" class="site-logo">
            <img src="{{asset('assets/img/applogo.jpg')}}"  class="img-fluid" style="width:90px;  height:auto; margin-top:-11px; margin-left:20px;" alt="image">
       
        </a>

        <button id="show-hide-sidebar-toggle" style="margin-left:80px; !important" class="show-hide-sidebar">
            <span>toggle menu</span>
        </button>

        <button class="hamburger hamburger--htla" >
            <span>toggle menu</span>
        </button>
        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="mobile-menu-right-overlay"></div>
                <div class="site-header-collapsed">
	                <div class="site-header-collapsed-in">
                    <a href="{{route('login')}}">
                        <button style="margin-top:-48px; margin-right:-150px; padding:10px 15px 10px 15px;"  class="btn btn-sm btn-rounded btn-danger btn-custom btn-inline float-right" type="button">
	                       Logout
                        </button>
                    </a>
                    </div>
                </div>
            </div><!--site-header-content-in-->
        </div><!--.site-header-content-->
    </div><!--.container-fluid-->
</header><!--.site-header-->
