@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; font-size:28px; margin-left:10px;">Business</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                            <div class="tbl-cell">
                                <a class="btn-sm btn btn-danger"  style="margin-right:-70px;"  href= "{{route('business')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i><span style= "font-size:20px;" > back</a>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered stripe row-border order-column table-striped " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Id</th>
                                <th>Phone Number</th>
                                <th>Date Of Birth</th>
                                <th>Gender</th>
                                <th>User Type</th>
                                <th>address</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $value)
                                <tr>
                                    <td>{{1}}  
                                    <td>{{$value->fname}}
                                    <td>{{$value->lname}}
                                    <td>{{$value->email}}
                                    <td>{{$value->phone_no}}
                                    <td>{{$value->dob}}
                                    <td>
                                        @if($value->gender == 'female')
                                            {{'Female'}}
                                        @else
                                            {{'Male'}}
                                        @endif
                                    <td>
                                        {{-- 1 for visitor --}}
                                        @if($value->user_type == 1)
                                            <span class="label label-success">{{'Visitors'}}</span>
                                        {{-- 2 for event --}}
                                        @elseif($value->user_type == 2)
                                            <span class="label label-primary">{{'Event User'}}</span>
                                        {{-- 3 for business --}}
                                        @else
                                            <span class="label label-info">{{'Business User'}}</span>
                                        @endif</td>
                                    <td>{{$value->address}}
                                    {{-- <td><a href ="{{route('userreg-delete',['id' => $value->user_id])}}">Delete</a> --}}
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection