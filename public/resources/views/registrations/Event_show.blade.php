@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2>Event Registration</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Id</th>
                                <th>Phone Number</th>
                                <th>Date Of Birth</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($event as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td>{{$value->fname}}
                                    <td>{{$value->lname}}
                                    <td>{{$value->email}}
                                    <td>{{$value->phone_no}}
                                    <td>{{$value->dob}}
                                    <td>{{$value->gender}}
                                    <td><a href ="{{route('event-delete',['id' => $value->event_id])}}">Delete</a>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection