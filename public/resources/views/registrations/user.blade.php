@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; font-size:28px; margin-left:10px;">User Details</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th> 
                                <th>Last Name</th>  
                                <th>User Type</th>
                                <th>Phone Number</th>
                                <th>Favourite Business</th> 
                                <th>Email Id</th>
                                <th>Date Of Registration</th>
                                <th>Gender</th>
                                <th>address</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($user as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td>{{$value->fname}}
                                    <td>{{$value->lname}}
                                    <td>
                                        @if($value->user_type == 1)
                                            <span class="label label-warning">{{'Event User'}}</span>
                                        @elseif($value->user_type == 2)
                                            <span class="label label-primary">{{'Business User'}}</span>
                                        @else
                                            <span class="label label-info">{{'Visitor User'}}</span>
                                        @endif</td>
                                    <td>{{$value->phone_no}}
                                    <td>@php
                                            $favourite  = App\Favourite::select('business_id')->where('user_id',$value->user_id)->count();
                                            //return $favourite;
                                        @endphp
                                        <a href = "{{route('fav-business',['id' => $value->user_id ])}}">
                                            <span class="label label-primary">{{$favourite}}</a></span>
                                    </td>
                                    <td>{{$value->email}}
                                    <td>{{$value->dob}}
                                    <td>
                                        @if($value->gender == 'female')
                                            <span class="label label-info">{{'Female'}}</span>   
                                        @else
                                        <span class="label label-warning">{{'Male'}}</span>
                                        @endif
                                    <td>{{$value->address}}
                                   
                                    </a>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection