@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding" >
        <h2 style="font-weight:510; margin-bottom:20px;  font-size:28px; margin-left:0px;">Send mail To Users</h2>
        <form id="myform" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <div class="col-lg-12">
                    <fieldset class="form-group">
                        <label class="form-label semibold" for="title">Subject</label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                        <small class="text-muted">Please give a subject to your Mail</small>
                    </fieldset>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-12">
                    <fieldset class="form-group">
                        <label class="form-label semibold" for="message">Description</label>
                        <textarea rows="4" name="message" id="message" class="form-control" placeholder="Message"></textarea>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; margin-bottom:20px;  font-size:28px; margin-left:0px;">Send User Mail</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                        </div>
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <a class="btn-sm btn btn-warning "  href= "{{route('usermail')}}"><span style= "font-size:20px;" >Users</a>
                                <a class="btn-sm btn btn-success "   href= "{{route('eventmail')}}"><span style= "font-size:20px;" >Event User</a>    
                                <a class="btn-sm btn btn-info "  href="{{route('businessmail')}}"><span style= "font-size:20px;" >Business User</a>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><input type="checkbox" id="ckbCheckAll"/></th>
                                <th>Name</th>
                                <th>Email</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td><input type ="checkbox" class="checkBoxClass" id="Checkbox1" name ="check" value ={{$value->user_id}}/>
                                    <td>{{$value->fname}}
                                    <td>{{$value->email}}
                                @endforeach
                            </tbody>
                        </table>
                        <a class="btn-sm btn btn-danger"  href= ""><span style= "font-size:20px;" >Send Email</a>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
