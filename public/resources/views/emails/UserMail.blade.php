@component('mail::message')
<small class="text-center">Thank You For Registration {{$data['fname']}}.</small>

{{-- @component('mail::button')
Button Text
@endcomponent  --}}

<a class=" text-center btn btn-primary">{{$data['code']}}</a>

Regards,<br>
<small class="text-center">{{ config('app.name') }}</small>
@endcomponent
