@component('mail::message')
Your New Password is
{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

{{-- <button>{{$data['code']}}</button> --}}
<a style="text-center" class="btn btn-primary">{{$data['code']}}</a>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
