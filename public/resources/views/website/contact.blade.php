@extends('website.inc.app')
@section('contact')
    <!-- contact -->
    <section id="contact1">
        <div class="content-box-md" style="padding:150px 0px 130px 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact-left inquiry-text" >
                            <div class="heading text-center">
                                <h1>Contact Us</h1>
                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum alias expedita tenetur in
                                molestiae suscipit, aut dolore sint odit beatae ad dolor aperiam qui repellendus.
                            </p>
                            
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div id="contact-right">
                            <form>
                                <h3 class="text-center">Send Message</h3>
                                <p class="text-center">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsum nulla illum, et nihil provident ducimus tenetur eveniet?</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name" placeholder="Your Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control add-email" id="e-mail" placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="phone" placeholder="Phone Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control add-email" id="Subject" placeholder="Subject">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control add" id="exampleFormControlTextarea1" rows="3" placeholder="Message"></textarea>
                                </div>
                                <div id="contact-btn">
                                    <a href="#" class="btn smooth-scroll btn-general btn-login" role="button">SUBMIT</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3"></div>

                </div>
            </div>
        </div>
    </section>
    <!-- contact ends -->
@endsection
    