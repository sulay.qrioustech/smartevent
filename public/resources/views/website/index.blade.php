@extends('website.inc.app')
@section('content')
    <!-- preloader -->
    <section id="preloader">
        <div id="pre">  
            &nbsp;
        </div>
    </section>
    <!-- preloader end -->

    <!-- content and image section -->
    <section id="home" class="owl-carousel owl-theme">
        <div class="card mb-3">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image5.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image6.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image3.jpg')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image1')}}" alt="Card image cap">
          </div>
    </section>

    <!-- upcomming and past event -->
    <section id="event">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-12">
                        <div id="event_left">
                            <div class="poster_image">
                                <img src="image/advertisement2.jpg" class="img-fluid imagespostar" alt="image">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-12">
                        <div id="event_right row">
                            <!-- upcomming -->
                            <div class="event_heading col-md-12">
                                <h4 class="text-left">Upcomming <span style="font-size:28px;"><b>Event</b></span></h4>
                            </div>
                            <div style="margin-left:20px !important;" class="upcommingevents owl-carousel owl-theme">
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                      <a href="singleEvent.html"><img src="image/kabirsingh.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Nakshatra</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/gameover.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Food</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/kabirsingh.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Anatarya</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/bharat.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Aroha</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/gameover.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Iskara</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                        <div class="col-md-3 col-12">
                            <div id="event_left">
                                <div id="event_left">
                                    <div class="poster_image" style="margin-top:70px;">
                                        <img src="image/advertisement6.jpg" class="img-fluid imagespostar1" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-12">
                            <div id="event_right row">
                                 <!-- past event -->
                                <div class="event_heading1 col-md-12">
                                    <!-- <h4 class="text-left" style="font-size:28px;"><span>Past </span>Event</h4> -->
                                    <h4 class="text-left">Past <span style="font-size:28px;"><b>Event</b></span></h4>
                                </div>
                                <div class="upcommingevents owl-carousel owl-theme">
                                    <div class="col-md-4 demo owl">
                                        <div class="event_image">
                                            <a href="singleEvent.html"><img src="image/kabirsingh.jpg" alt="images"/></a>
                                        </div>
                                        <div class="image_description">
                                            <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                            <div class="heading"><p>Nakshatra</p></div>
                                            <div class="date">Dec 5 2019</div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4 demo owl">
                                        <div class="event_image">
                                            <a href="singleEvent.html"> <img src="image/gameover.jpg" alt="images"/></a>
                                        </div>
                                        <div class="image_description">
                                            <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                            <div class="heading"><p>Food</p></div>
                                            <div class="date">Dec 5 2019</div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4 demo owl">
                                        <div class="event_image">
                                            <a href="singleEvent.html"> <img src="image/kabirsingh.jpg" alt="images"/></a>
                                        </div>
                                        <div class="image_description">
                                            <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                            <div class="heading"><p>Anatarya</p></div>
                                            <div class="date">Dec 5 2019</div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4 demo owl">
                                        <div class="event_image">
                                            <a href="singleEvent.html"><img src="image/bharat.jpg" alt="images"/></a>
                                        </div>
                                        <div class="image_description">
                                            <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                            <div class="heading"><p>Aroha</p></div>
                                            <div class="date">Dec 5 2019</div>
                                        </div> 
                                    </div>
                                    <div class="col-md-4 demo owl">
                                        <div class="event_image">
                                            <a href="singleEvent.html"><img src="image/gameover.jpg" alt="images"/></a>
                                        </div>
                                        <div class="image_description">
                                            <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                            <div class="heading"><p>Iskara</p></div>
                                            <div class="date">Dec 5 2019</div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcomming and past event ends -->

    <!-- testimoinial section -->
    <section id="testimonials">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="testimoinial_heading">See What <span style="color: #CD2129; font-size: 40px;"><b>Our</b></span> Clients Say <span style="color: #CD2129; font-size: 40px;"><b>About Us</b></span></h2>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class=""></li>
                            <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="item carousel-item">
                            <div class="row">
                                <div class="col-sm-6 ">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime amet commodi aperiam cum ad! Dolor exercitationem illo dolorem voluptatem deserunt, illum rem blanditiis magnam dolore hic repellendus voluptatibus, similique rerum."</div>
                                    <div class="media">
                                    <div class="media-left d-flex mr-3">
                                        <img src="image/team-1.jpg" class="rounded-circle img-fluid" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="overview">
                                        <div class="name text-left"><b>Stuart Rox</b></div>
                                        <div class="details text-left">Founder / CEO</div>
                                        </div>										
                                    </div>
                                    </div>
                                </div>								
                                </div>
                                <div class="col-sm-6 ">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime amet commodi aperiam cum ad! Dolor exercitationem illo dolorem voluptatem deserunt, illum rem blanditiis magnam dolore hic repellendus voluptatibus, similique rerum."
                                    </div>
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <img src="image/team-1.jpg" class="rounded-circle img-fluid" alt="">	
                                        </div>
                                        <div class="media-body">
                                            <div class="overview">
                                            <div class="name text-left"><b>Lorel Parker</b></div>
                                            <div class="details text-left">CTO</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>								
                                </div>
                            </div>			
                            </div>
                            <div class="item carousel-item active"> 
                            <div class="row">
                                <div class="col-sm-6 owl">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime amet commodi aperiam cum ad! Dolor exercitationem illo dolorem voluptatem deserunt, illum rem blanditiis magnam dolore hic repellendus voluptatibus, similique rerum."</div>
                                    <div class="media">
                                    <div class="media-left d-flex mr-3">
                                        <img src="image/team-1.jpg"  class="rounded-circle img-fluid" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="overview">
                                        <div class="name text-left"><b>Shota Oda</b></div>
                                        <div class="details text-left">CEO</div>
                                        </div>
                                    </div>
                                    </div>
                                </div>								
                                </div>
                                <div class="col-sm-6">
                                <div class="testimonial-wrapper">
                                    <div class="testimonial">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime amet commodi aperiam cum ad! Dolor exercitationem illo dolorem voluptatem deserunt, illum rem blanditiis magnam dolore hic repellendus voluptatibus, similique rerum."</div>
                                    <div class="media">
                                    <div class="media-left d-flex mr-3">
                                        <img src="image/team-1.jpg" class="img-fluid rounded-circle" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="overview">
                                        <div class="name text-left"><b>Shin Paul</b></div>
                                        <div class="details text-left">Co-Founder / Managing Director</div>
                                        </div>										
                                    </div>
                                    </div>
                                </div>								
                                </div>
                            </div>			
                            </div> 
                        </div> 
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </section>
    <!-- testimoinial section ends-->

    <!-- counter section -->
    <section id="counter-stats" class="wow fadeInRight" data-wow-duration="1.4s">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 text-center stats">
                        <i class="fa fa-user"  aria-hidden="true"></i>
                        <div class="counting" data-count="100">100</div>
                        <h5>Event Users</h5>
                        <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                        </p>
                    </div>
                    <div class="col-lg-4 text-center stats">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        <div class="counting" data-count="250">250</div>
                        <h5>Business Users</h5>
                        <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                            </p>
                    </div>
                    <div class="col-lg-4 text-center stats">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <div class="counting" data-count="20">20</div>
                        <h5>Users</h5>
                        <p class="description">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                            </p>
                    </div>
                </div>
            </div>        
        </div>
    </section>
    <!-- counter section ends -->

    <!-- inquiry -->
    <section id="inquiry">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 inquiry-info">
                        <h2 class="inquiry_heading">Inquiry</h2>
                        <p>Contact us even if you have the slightest of question in mind regarding anything.</p>
                    </div>
                    <div class="col-md-5">
                        <a href="" style="margin-left:20px;" class="btn btn-lg btn-custom" data-target="#inquiry1" data-toggle="modal"><i class="fa fa-forward" aria-hidden="true"></i> Request a Free Quote
                        </a>
                        <!-- <button style="margin-left:20px;" type="submit"  class="btn btn-lg btn-custom" data-target="#inquiry" data-toggle="modal"><i class="fa fa-forward" aria-hidden="true"></i> &nbsp;Request a Free Qoute</button> -->
                    </div>
                    <div id="inquiry1" style="margin-top: 50px;" class="modal fade" role="dialog">
                        <div class="modal-dialog"> 
                            <div class="modal-content">
                            <div class="modal-body">
                                <button data-dismiss="modal" class="close btn-close">&times;</button>
                                <h4>Inquiry</h4>
                                <form>
                                <input type="text" name="username" class="form-control" placeholder="Name">
                                <input type="text" name="EmailId" class=" form-control" placeholder="Email Address"/>
                                <input type="text" name="password" class=" form-control" placeholder="Phone Number"/>
                                <textarea class=" form-control" placeholder="Message"></textarea>
                                
                                <input class="btn-login" type="submit" value="Submit"/>
                                </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </section>
    <!-- inquiry ends -->

    <!-- email address callus -->
    <section id="contact-us">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="info">
                            <i class="fa fa-envelope-o icons" style="color:burlywood;" aria-hidden="true"></i> &nbsp;Email
                            <p>abc@gmail.com</p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="info">
                            <i class="fa fa-map-marker icons" style="color:#F04E39;" aria-hidden="true"></i> &nbsp;Address
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="info">
                            <i class="fa fa-mobile icons" style="color:goldenrod; font-size:40px;" aria-hidden="true"></i>&nbsp;Call Us
                            <p style="font-family: 'Ruda',sans-serif; font-weight:800;">5454543424</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- email address callus ends-->
@endsection

@section('scripts')
    <script>
        let user_id ='{{$user->user_id}}'
        
        $(function(){
            $(document).on('submit','#create_event',function(e){
                e.preventDefault();
                console.log('hii');
                $.ajax(
                    {
                        url: "http://smartevent.test/api/create/event",
                        type: "POST",
                        data: new FormData(this),
                        dataType: "json",
                        processData:false,
                        contentType:false,
                        success: function(data){
                            let response = data;
                            console.log(response);

                            if(response.status == 1)
                            {
                                $('.alert-success').removeClass('display');
                                $('.alert-success').addClass('visible');
                                window.location.href = '/index/'+user_id;
                            }   
                            else
                            {
                                $('.alert-danger').removeClass('display');
                                $('.alert-danger').addClass('visible');
                            }
                        }
                    }
                );
            });
        });

        $(function(){
            $.ajax(
                {
                    url : "http://smartevent.test/api/upcommingEvent",
                    method : "get",
                    dataType: "json",
                    success : function(data){
                        let response = data.data;
                        console.log(response);
                        
                        $('.upcommingevents').html('');
                        $.each(response,function(index,item){
                            $('.upcommingevents').append(`
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="${item.logo_image}" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>${item.event_name}</p></div>
                                        <div class="date">${item.start_date}</div>
                                    </div> 
                                </div>
                            `);
                        });
                    }
                }
            );
        });

    </script>
@endsection