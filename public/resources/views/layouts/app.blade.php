<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sultani Makutano</title>
	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="{{asset('assets/img/applogo.jpg')}}" rel="shortcut icon">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="{{asset('/assets/css/lib/lobipanel/lobipanel.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/separate/vendor/lobipanel.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/lib/jqueryui/jquery-ui.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/separate/pages/widgets.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/lib/bootstrap/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/lib/datatables-net/datatables.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/separate/vendor/datatables-net.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/separate/vendor/tags_editor.min.css')}}">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="{{asset('/assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/separate/vendor/select2.min.css')}}" >
	<link rel="stylesheet" href="{{asset('/assets/css/main.css')}}">
	<link rel="stylesheet" href="{{asset('/assets/css/app.css')}}">	
</head>
<body class="with-side-menu control-panel control-panel-compact">
    @include('inc/navbar')
	@include('inc/sidebar')
	<div class="page-content">
			<div class="container-fluid">
			{{-- -	@include('message.error') --}}
				@yield('content')
			</div><!--.container-fluid-->
	</div><!--.page-content-->
	<script src="{{asset('/assets/js/lib/jquery/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/popper/popper.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/tether/tether.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap/bootstrap.min.js')}}"></script>
	<script src="{{asset('/assets/js/plugins.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/lib/jqueryui/jquery-ui.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/lib/lobipanel/lobipanel.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/datatables-net/datatables.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/jquery-tag-editor/jquery.caret.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/jquery-tag-editor/jquery.tag-editor.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-select/bootstrap-select.min.js')}}"></script>	
	<script src="{{asset('/assets/js/lib/select2/select2.full.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/notie/notie.js')}}"></script>
	<script src="{{asset('/assets/js/lib/notie/notie-init.js')}}"></script>
	<script src="{{asset('/assets/js/lib/pnotify/pnotify.js')}}"></script>
	<script src="{{asset('/assets/js/lib/pnotify/pnotify-init.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-notify/bootstrap-notify-init.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-table/bootstrap-table.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-table/bootstrap-table-export.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-table/tableExport.min.js')}}"></script>
	<script src="{{asset('/assets/js/lib/bootstrap-table/bootstrap-table-init.js')}}"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>	

	<script>
		// $(function() {
		// 	$('#tags-editor-textarea').tagEditor();
		// });
		// $(function() {	
		// 	$('#example').DataTable();
		// })
		
		$(function() {
			$('#example').DataTable({
				responsive: true
			});
		});

	
		$(function() {
			$('#tags-editor-textarea').tagEditor();
		});
	
		
	</script>
	<script src="{{asset('/assets/js/app.js')}}"></script>	
@yield('scripts')

</body>
</html>

