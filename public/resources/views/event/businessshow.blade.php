@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; font-size:28px; margin-left:10px;">Business</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                            <div class="tbl-cell">
                                <a class="btn-sm btn btn-danger"  style="margin-right:-72px;"  href= "{{route('event')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i><span style= "font-size:20px;" > back</a>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>logo Image</th>
                                <th>Approve Status</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($business as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td>{{$value->company_name}}
                                    <td><img src="{{asset('storage/images/'.$value->logo_image)}}" class="img-fluid" style="height: 60px; width:60px;" alt="image">
                                    <td>@php
                                         $event = App\event_business::where(['event_id' => $id,'business_id'=> $value->business_id ])->first();
                                        @endphp
                                        @if($event->business_approve_status == 0)
                                            <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">Pending</a>
                                            {{-- modal --}}
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are You Sure Want to Approve?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-sm btn-secondary">Cancle</a>
                                                        <a class="btn btn-sm btn-danger" href="{{route('event-business-approve',['event_id' => $id,'business_id'=>$value->business_id ])}}">Approve</a>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        @else
                                            <a class="btn btn-sm btn-primary">Approve</a>   
                                        @endif
                                    <td>{{$value->description}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection