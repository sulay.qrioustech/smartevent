@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; font-size:28px; margin-left:10px;">Events</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Event Name</th>
                                <th>logo</th>
                                <th>Business Count</th>
                                <th>Approve Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Address</th>
                                <th>Description</th>
                                <th>Phone Number</th>  
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($event as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td>{{$value->event_name}}
                                    <td><img src="{{asset('storage/images/'.$value->logo_image)}}" class="img-fluid" style="height: 60px; width:60px;" alt="image">
                                    <td>@php
                                            $business_count = App\event_business::with('business')->where('event_id',$value->event_id)->count();
                                        @endphp 
                                        <a href="{{route('event-business-view',['id' => $value->event_id])}}">
                                            <span class="label label-primary">{{$business_count}}</span></a> 
                                    </td>           
                                    <td>
                                        @if($value->event_status == 0)
                                            <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal{{$value->event_id}}" >Pending</a>
                                           
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal{{$value->event_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are You Sure Want to Approve?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-sm btn-secondary">Cancle</a>
                                                        <a class="btn btn-sm btn-danger" href="{{route('event-approve',['id' => $value->event_id])}}">Approve</a>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        @else
                                            <a class="btn btn-sm btn-primary" href="{{route('event-disapprove',['id' =>
                                                $value->event_id])}}" data-toggle="modal" data-target="#exampleModall{{$value->event_id}}" >Approve</a>   


                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModall{{$value->event_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are You Sure Want to Dis-Approve?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-sm btn-secondary">Cancle</a>
                                                        <a class="btn btn-sm btn-danger" href="{{route('event-disapprove',['id' => $value->event_id])}}">DisApprove</a>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        @endif

                                    <td>{{$value->start_date}}
                                    <td>{{$value->end_date}}
                                    <td>{{$value->start_time}}
                                    <td>{{$value->end_time}}
                                    <td>{{$value->address}}
                                    <td>{{$value->description}}
                                    <td>{{$value->phone_number}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

