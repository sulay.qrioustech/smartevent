@extends('layouts/app')
@section('content')
    <div class="box-typical box-typical-padding">
        <h2 class="m-t-lg with-border" style="font-weight:510; font-size:28px; margin-left:10px;">About Us</h2>
         <form method="post" action="{{route('about-update',['id'=>$about->about_id])}}">
            {{csrf_field()}}
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Description</label>
                <div class="col-sm-10">
                    <textarea rows="4" class="form-control" name="description" required="true" placeholder="textarea">{{$about->description}}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="button" style="padding:10px 15px 10px 15px;" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-inline btn-danger" value="Submit"/>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit About Us</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <p>Are You Sure Want to Update? </p>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Save Changes</button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>  

        </form>
    </div>
@endsection
        