<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sultani Makutano- Admin Panel</title>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="{{asset('/assets/css/separate/pages/login.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/lib/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/main.css')}}">
</head>
<body>
    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box box" method="POST" style="box-shadow:0px 1px 10px 1px #C02964; border:none;" action="{{route('signin')}}">
                    {{csrf_field()}}
                    <div class="sign-avatar float-center">
                        <img src="{{asset('assets/img/applogo.jpg')}}" style=" margin-left:-40px; width:150px; height:100px;" alt="image">
                    </div>
                    <header class="sign-title"><b>Sign In</b></header>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="E-Mail or Phone"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control"name="password" placeholder="Password"/>
                    </div>
                    <button type="submit"  class="btn btn-sm btn-rounded btn-danger">Sign in</button>
                    <!--<button type="button" class="close">
                        <span aria-hidden="true">&times;</span>
                    </button> id="bn-passing-title" -->
                    {{-- <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-info animated fadeInDown" role="alert" data-notify-position="top-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 1031; top: 160px; right: 20px; animation-iteration-count: 1;"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button><span data-notify="icon"></span> <span data-notify="title">Welcome:</span> <span data-notify="message">Thank you for successfull login</span><a href="#" target="_blank" data-notify="url"></a></div> --}}
                </form>
            </div>
        </div>
    </div><!--.page-center-->


<script src="{{asset('/assets/js/lib/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('/assets/js/lib/popper/popper.min.js')}}"></script>
<script src="{{asset('/assets/js/lib/tether/tether.min.js')}}"></script>
<script src="{{asset('/assets/js/lib/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('/assets/js/plugins.js')}}"></script>

<script src="{{asset('/assets/js/lib/notie/notie.js')}}"></script>
<script src="{{asset('/assets/js/lib/notie/notie-init.js')}}"></script>
<script src="{{asset('/assets/js/lib/pnotify/pnotify.js')}}"></script>
<script src="{{asset('/assets/js/lib/pnotify/pnotify-init.js')}}"></script>
<script src="{{asset('/assets/js/lib/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('/assets/js/lib/bootstrap-notify/bootstrap-notify-init.js')}}"></script>

<script type="text/javascript" src="{{asset('/assets/js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });

        // $(function() {
        //     $('#bn-passing-title').click(function(){
        //         var newDiv = $('<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-info animated fadeInDown" role="alert" data-notify-position="top-right" style="display: inline-block; margin: 0px auto; position: fixed; transition: all 0.5s ease-in-out 0s; z-index: 1031; top: 160px; right: 20px; animation-iteration-count: 1;"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button><span data-notify="icon"></span> <span data-notify="title">Welcome:</span> <span data-notify="message">Successfull login</span><a href="#" target="_blank" data-notify="url"></a></div>');
        //     //newDiv.style.background = "#000";
        //     $('body').append(newDiv);
        //     });
        // });
    </script>
<script src="{{asset('/assets/js/app.js')}}"></script>
</body>
</html>

{{-- <span data-notify="message">This plugin has been provided to you by Robert McIntosh aka mouse0270</span> --}}

{{--  --}}