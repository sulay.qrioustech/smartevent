//preloader
$(window).on('load',function(){
    $("#pre").fadeOut();
    $("#preloader").delay(100).fadeOut();
});

// $('.owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
//     responsive:{
//     0:{
//         items:1
//     },
//     600:{
//          items:1
//      },
//         1200:{
//             items:1
//         }
//     }
//     // responsive:{
//     //     0:{
//     //         items:1
//     //     },
//     //     600:{
//     //         items:3
//     //     },
//     //     1000:{
//     //         items:5
//     //     }
//     // }
// });
// $(' #home .owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:1
//         },
//         1200:{
//             items:1
//         },
//     }
// });




// $('#event .owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
//     responsive:{
//         0:{
//             items:2
//         },
//         576:{
//             items:3
//         },
//         600:{
//             items:3
//         },
//         768:{
//             items:2
//         },
//         900:{
//             items:2
//         },
//         1024:{
//             items:3
//         },
//         1200:{
//             items:3
//         },
//         1400:{
//             items:3
//         },
//     }
// });

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:1,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1200:{
            items:1
        },
    }
});

$(function(){
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
}); 



$(function(){
    $('.navbar-toggler-icon').on('click',function(e){
       $('.navbar-toggler-icon') .hide();
       $('.btn-close').show();
       $('#navbarNavDropdown').show();
    }); 
});

$(function(){
    $('.btn-close').on('click',function(e){
        $('.btn-close').hide();
       $('.navbar-toggler-icon') .show();    
       $('#navbarNavDropdown').hide();
    }); 
});

$(function(){
    $('#navbarDropdownMenuLink').on('click',function(e){
        $('.btn-close').hide();
        $('#navbarNavDropdown').hide();
        $('.navbar-toggler-icon') .show();    
    });
});

$(function(){
    $('.nav-link').on('click',function(e){
        $('.btn-close').hide();
        $('#navbarNavDropdown').hide();
        $('.navbar-toggler-icon') .show();    
    });
});

$(document).on('change','.up', function(){
    var names = [];
    var length = $(this).get(0).files.length;
     for (var i = 0; i < $(this).get(0).files.length; ++i) {
         names.push($(this).get(0).files[i].name);
     }
     // $("input[name=file]").val(names);
     if(length>2){
       var fileName = names.join(', ');
       $(this).closest('.form-group').find('.form-control').attr("value",length+" files selected");
     }
     else{
         $(this).closest('.form-group').find('.form-control').attr("value",names);
     }
});