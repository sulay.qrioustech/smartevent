$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        576:{
            items:2
        },
        1200:{
            items:3
        },
    }
});

$(function(){
    $('.navbar-toggler-icon').on('click',function(e){
       $('.navbar-toggler-icon').hide();
       $('.single-event').hide();
       $('.btn-close').show();
       $('#navbarNavDropdown').show();
    }); 
});

$(function(){
    $('.btn-close').on('click',function(e){
        $('.single-event').show();
        $('.btn-close').hide();
       $('.navbar-toggler-icon') .show();    
       $('#navbarNavDropdown').hide();
    }); 
});

$(function(){
    $('#navbarDropdownMenuLink').on('click',function(e){
        $('.navbar-toggler-icon').hide();
        $('.btn-close').hide();
        $('#navbarNavDropdown').hide();
        $('.navbar-toggler-icon') .show();    
    });
});

$(function(){
    $('.nav-link').on('click',function(e){
        $('.navbar-toggler-icon').show();
        $('.btn-close').hide();
        $('#navbarNavDropdown').hide();
        $('.navbar-toggler-icon') .show();    
    });
});