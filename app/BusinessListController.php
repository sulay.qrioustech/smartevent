<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusinessCreate;
use App\User_Registration;
use App\Favourite;
use App\EventCreate;
use App\event_business;

class BusinessListController extends Controller
{
    public function getBusinessList(Request $request)
    {
        try
        {
            $business = BusinessCreate::with(['user','user_favourite' => function($query) use($request) {
                return $query->where('user_id',$request->user_id);
            }])->where('business_status',1)->get();

            $image_path =[];
            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($business as $value)
            {
                //return $value->logo_image;
                $value->logo_image= url('/').$path.$value->logo_image;    
                $imagess = json_decode($value->images);
                //return $imagess;
                foreach($imagess as $value)
                 {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $business[$i]['images'] = $image_path;
                $i++;
            }  

            if($business)
            {
                $response = [
                    'msg' => 'successfull transfer business list',
                    'data' => $business,
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'cant approve business by admin',
                    'status'=> 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
    
    public function singleBusinessDetail(Request $request)
    {
        try
        {  
            $business_id = $request->business_id;
            $business = BusinessCreate::with('user')->find($business_id);
            
            $image_path =[];
            $path = config('CustomVariable.image_dir');
            $business->logo_image = url('/').$path.$business->logo_image;    
            
            $imagess = json_decode($business->images);
            //return $imagess;
            foreach($imagess as $value)
                {
                $temp = url('/').$path.$value;
                array_push($image_path,$temp);
            }
            $business['images'] = $image_path;
               
           
            
            if($business)
            {
                $response = [
                    'msg' => 'Successfull businessdetail transfer',
                    'data' => $business,
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'Business_id doesnot exist',
                    'status' => 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];   
        }
        return response()->json($response);
    }

    public function selectEvent(Request $request)
    {
        try
        {
            $event_id = $request->event_id;
            $business_id = $request->business_id;
            $event = EventCreate::find($event_id);

            if($event)
            {
                $event_business = new event_business;
                $event_business->event_id = $event_id;
                $event_business->business_id = $business_id;
                $event_business->save();

                $response = [
                    'msg' => 'successfull registerd in event',
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function selectEventList(Request $request)
    {
        try
        {   
            // $business_id = $request->business_id;
            // $event = BusinessCreate::with(['events' => function($query){
            //     return $query->where('event_status','=',1);
            // }])->find($business_id);
        
            $event = event_business::with('events')->where('business_id',$request->business_id)->get();
            //return $event;
            $image_path =[];
            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($event as $value)
            {
                //return $value->logo_image;
                $value->events->logo_image= url('/').$path.$value->events->logo_image;    
                $imagess = json_decode($value->events->images);
                foreach($imagess as $value)
                 {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $event[$i]['events']['images'] = $image_path;
                $i++;
            }  

            if($event)
            {
                $response = [
                    'event_list' => $event,
                    'msg' => 'successfull events',
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg'=>'admin does not approve event',
                    'status'=> 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }

    public function favouriteBusiness(Request $request)
    {
        try
        {
            //return json_encode($request->business_id);
            $user = $request->user_id;
            $business = $request->business_id;
            $favourite = Favourite::where(['business_id' => $request->business_id ,'user_id' =>$request->user_id ])->first();
            if($favourite)
            {
                $response = [
                    'msg' => 'Cant add favourite',
                    'status' => 0
                ];   
            }
            else
            {
                $favourite = new Favourite;
                $favourite->user_id = $request->user_id;
                $favourite->business_id = $request->business_id;
                // $favourite->event_id = $request->event_id;
                $favourite->save();
                
                if($favourite->save())
                {
                    $response = [
                        'favourite_id' => $favourite->favourite_id,
                        'msg' => 'Successfull add favourite',
                        'status' => 1
                    ]; 
                }
                else
                {
                    $response = [
                        'msg' => 'Cant add favourite',
                        'status' => 0
                    ];   
                }
            }   
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function getfavouriteBusineess(Request $request)
    {
        try
        {
            $business_id = Favourite::with(['business'=>function($query){
                return $query->where(['business_status'=>1]);
            },'business.user'])->where('user_id',$request->user_id)->get();
          
            $response = [
                'data' => $business_id,    
                'msg' => 'Successfull find list',
                'status' => 1
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function deletefavourites(Request $request)
    {
        try
        {
            $favourite_id = $request->favourite_id;
            $favourite = Favourite::find($favourite_id);
            $favourite->delete();    
    
            $response = [
                'msg' => 'Successfull Unfavourite',
                'status' => 1
            ];
        
         
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
}
