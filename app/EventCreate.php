<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCreate extends Model
{
    public $table = 'event';

    protected $primaryKey = 'event_id';

    public $timestamps = true;  

    public function business()
    {
        return $this->belongsTo('App\BusinessCreate','business_id');
    }

    public function hasbusiness()
    {
        return $this->belongsTo('App\event_business','event_id');
    }
}
