<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UploadImage extends Model
{
    public static function Image(Request $request, $images)
    {  
        $images = $request->file('images');
        $data = 'ABCDEFGH12389IRSTUVWXYZ4567JKLMNOPQ';
        $temp = [];

        foreach($images as $image)
        {
            $i = substr(str_shuffle($data), 0, 7);
            $fileNameWithExt = $image->getClientOriginalName();
            $fileNameWithExt = str_replace(" ", "_", $fileNameWithExt);
    
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $filename = preg_replace("/[^a-zA-Z0-9\s]/", "", $filename);
            $filename = urlencode($filename);
    
            $extension = $image->getClientOriginalExtension();
    
            $fileNameToStore = $filename.$i.'.'.$extension;
            
            $path = $image->storeAs('public/images',$fileNameToStore);

            array_push($temp,$fileNameToStore);
        }
        return $temp;
    }

    public static function Image1(Request $request, $logo_image)
    {  
         //image Extension
         $ImageExtension=$request->file('logo_image')->getClientOriginalName();
         // get image
         $ImageName=pathinfo($ImageExtension,PATHINFO_FILENAME);
         //get Extension
         $Extension=$request->file('logo_image')->getClientOriginalExtension();
         //name
         $ImageNameToStore=$ImageName.'_'.time().'.'.$Extension;
         //path
         $path=$request->file('logo_image')->storeAs('public/images',$ImageNameToStore);

         return $ImageNameToStore;
    }
}
