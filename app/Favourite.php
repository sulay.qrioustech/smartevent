<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    public $table = "favourite_business";   

    protected $primaryKey = "favourite_id";

    public $timestamps = true;

    public function business()
    {
        return $this->belongsTo('App\BusinessCreate','business_id');
    }
}
