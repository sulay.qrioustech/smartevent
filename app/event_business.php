<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event_business extends Model
{
    public $table = "event_business";

    protected $primaryKey = "event_business_id";

    public $timestmaps  = true;

    public function events()
    {
        return $this->belongsTo('App\EventCreate','event_id');
    }

    public function business()
    {
        return $this->belongsTo('App\BusinessCreate', 'business_id');
    }
}
