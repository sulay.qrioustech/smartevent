<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Registration extends Model
{
    public $table = 'user_registration';

    protected $primaryKey = "user_id";

    public $timestamps = true;


    public function business()
    {
        return $this->hasMany('App\BusinessCreate','user_id');
    }
}
