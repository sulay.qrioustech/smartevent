<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_Registration;
use App\emails;
use App\Jobs\sendEmail;
use App\Mail\sendUserEmail;
use Mail;

class SendEmailController extends Controller
{
    public function view()
    {
        $user = User_Registration::all();
        $users = User_Registration::where('user_type',1)->get();
        $event_users = User_Registration::where('user_type',2)->get();
        $business_users = User_Registration::where('user_type',3)->get();
        //return view('sendemail/email')->with('user',$user);
        return view('sendemail/email',compact('user','users','event_users','business_users'));
    }

    // public function user_show()
    // {
    //     $users = User_Registration::where('user_type',1)->get();
    //     return view('sendemail/usermail')->with('users',$users);
    // }

    // public function event_show()
    // {
    //     $users = User_Registration::where('user_type',2)->get();
    //     return view('sendemail/eventmail')->with('users',$users);
    // }

    // public function business_show()
    // {
    //     $users = User_Registration::where('user_type',3)->get();
    //     return view('sendemail/businessmail')->with('users',$users);
    // }

    public function sendAllUserMail(Request $request)
    {
        //dd($request->all());
        //dd($request->checkbox);
        $email = new emails;
        $email->subject = $request->subject;
        $email->description = $request->message;
        $email->user_id = json_encode($request->checkbox);
        $email->save();

        $user_email = User_Registration::select('email')->whereIn('user_id',$request->checkbox)->get();
        //return $user_email;

        foreach($user_email as $value)
        {
            $data = [
                'subject' => $request->subject,
                'description' => $request->description,
                'email' => $value->email
            ];
    
            dispatch((new sendEmail($data)));
        }
        return redirect()->route('email');
    }
}
