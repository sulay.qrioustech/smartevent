<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;


class LoginController extends Controller
{
    public function display()
    {
        return view('login/signin');
    }
    
    public function login(Request $request)
    {
        try{
            $validator = Validator :: make($request->all(),[
                'email' => 'required',
                'password' => 'required'
            ]);

            if($validator->fails())
            {
                return redirect()->route('login');
            }
            else
            {
                if(Auth::attempt(['email' => $request->email , 'password' => $request->password]))
                {
                    return redirect()->route('dashboard');
                }
                else
                {
                   return redirect()->route('login');
                   //return 'hii';
                }
            }
        }   
        catch(\Exception $e)
        {
            return $e->getMessage()." ".$e->getLine()." ".$e.getFile();   
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
