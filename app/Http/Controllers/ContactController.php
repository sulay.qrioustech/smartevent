<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Validator;
use App\Mail\ContactMail;  

class ContactController extends Controller
{
    public function insert(Request $request)   
    {
        try{
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'email'=>'required | email',
                'message'=>'required'
            ]);

            if($validator->fails())
            {
                $response = [
                    'msg' => 'every field is required',
                    'status' => 0
                ];
            }
            else
            {
                $data  = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'message' => $request->message
                ];
                      
                $event = new Contact;
                $event->name = $request->name;
                $event->email = $request->email;
                $event->message = $request->message; 
                $event->save();

                Mail::to($request->email)->send(new EventMail($data));

                $response = [
                    'msg' => 'Contact Registration Successfull',
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }   
    }   

    public function display()
    {
        $contact =  Contact::all();
        return view('registrations/contact')->with('contact',$contact);
    }

    public function delete($id)
    {
        $contact = Contact :: find($id);
        $contact->delete();
        return redirect()->route('contact');
    }
}


