<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_Registration;
use Validator;
use Mail;
use App\Mail\UserMail;
use Hash;
use App\Favourite;
use App\BusinessCreate;

class UserController extends Controller
{
    public function registration(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required',
                'password' => 'required',
                'phone_no' => 'required',
                'gender' => 'required',
                'dob' => 'required',
                'user_type' => 'required',
                'address' => 'required',
            ]);

            if($validator->fails())
            {   
                $response = [
                    'msg' => 'Every field is required',
                    'status' => 0,
                ];
            }
            else
            {
                $data = [
                    'fname' => $request->fname,
                    'lname' => $request->lname,
                    'email' => $request->email,
                    'password' => $request->password,
                    'phone_no' => $request->phone_no,
                    'gender' => $request->gender,
                    'dob' => $request->dob,
                    'user_type' => $request->user_type,
                    'address' => $request->address
                ];

                $user = new  User_Registration;
                $user->fname = $request->fname;
                $user->lname = $request->lname;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->phone_no = $request->phone_no;
                $user->gender = $request->gender;
                $user->dob = $request->dob;
                $user->user_type = $request->user_type;
                $user->address = $request->address;
                $user->save();

                Mail::to($user->email)->send(new UserMail($data));
            }
        }
        catch(\Exception $e)
        {   
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0,
            ];
        }
        
    }

    public function display()
    {
        $user = User_Registration::all();
        return view('registrations/user')->with('user',$user);
    }

    public function favourite_business($id)
    {   
        $favouite_busienss = Favourite::select('business_id')->where('user_id',$id)->get();
        //$business = BusinessCreate::where('business_id',$favouite_busienss)->get();
        $business = BusinessCreate::find($favouite_busienss);
        return view('user/favourite')->with('business',$business);
    }

    public function delete($id)
    {
        $user = User_Registration :: find($id);
        $user->delete();
        return redirect()->route('userreg-show');
    }
}


