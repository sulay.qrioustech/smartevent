<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AboutUs;
use Validator;

class AboutController extends Controller
{
    public function add()
    {
        return view('about_us/insert');
    }

    public function insert(Request $request)
    {
        try{
            $about = new AboutUs;
            $about->description = $request->description;
            $about->save();
            return redirect()->route('about-show');
        }
        catch(\Exception $e)
        {
            return $e.getLine()." ".$e.getMessage()." ".$e->getFile();
        }
    }

    public function show()
    {
        try{
            $about = AboutUs::all();
            return view('about_us/about')->with('about',$about);
        }
        catch(\Exception $e)
        {
            return $e.getLine()." ".$e.getMessage()." ".$e->getFile();
        }
    }

    public function edit()
    {
        $about = AboutUs::find(1);
        return view('about_us/edit')->with('about',$about);
    }

    public function update(Request $request,$id)
    {
        $about = AboutUs::find($id);
        $about->description = $request->description;
        $about->save();
        return redirect()->route('about-edit');
    }

    // public function delete($id)
    // {
    //     $about = AboutUs::find($id)->delete();
    //     return redirect()->route('about-show');
    // }
}
    

