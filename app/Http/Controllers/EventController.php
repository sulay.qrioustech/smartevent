<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Storage;
use App\EventCreate;
use App\BusinessCreate;
use App\event_business;

class EventController extends Controller
{  
    public function view()
    {
        try
        {
            $event = EventCreate::all();
            return view('event/show')->with('event',$event);
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
    }    

    public function showBusiness($id)
    {
        $business_id = event_business::select('business_id')->where('event_id',$id)->get();
        $business = BusinessCreate::find($business_id);
        return view('event/businessshow')->with(['business'=>$business,'id'=>$id]);
    }

    public function delete($id)
    {
        $event = EventCreate::find($id);
        $event->delete();   
        Storage::delete('/storage/images/'.$event->logo_image);
        return redirect()->route('event');
    }

    public function approve($id)
    {
        $event = EventCreate::find($id);
        $event->event_status = 1;
        $event->save();
        return redirect()->route('event');
    }

    public function disapprove($id)
    {
        $event = EventCreate::find($id);
        $event->event_status = 0;
        $event->save();
        return redirect()->route('event');
    }
    
    public function businessApprove($event_id,$business_id)
    {
        $event = event_business::where(['event_id' => $event_id,'business_id'=> $business_id])->first();
        $event->business_approve_status = 1;
        $event->save();
        return redirect()->route('event');   
    }
}   

