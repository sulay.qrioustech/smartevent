<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_Registration;
use App\EventCreate;
use App\BusinessCreate;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function display()
    {
        try
        {
            $data = [
                'today' => User_Registration::whereDate('created_at', Carbon::today())->count(),
                'week' => User_Registration::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count(),
                'month' => User_Registration::where('created_at', '>=', Carbon::now()->startOfMonth())->count(),
                'event_today'=> EventCreate::whereDate('created_at', Carbon::today())->count(),
                'event_week' => EventCreate::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count(),
                'event_month' => EventCreate::where('created_at', '>=', Carbon::now()->startOfMonth())->count(),
                'business_today' => BusinessCreate::whereDate('created_at', Carbon::today())->count(),
                'business_week' => BusinessCreate::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count(),
                'business_month' => BusinessCreate::where('created_at', '>=', Carbon::now()->startOfMonth())->count()
            ];
    
            return view('dashboard/dashboard')->with($data);
        }
        catch(\Exception $e)
        {
            return $e->getMessage()." ".$e->getFile()." ".$e->getLine();
        }
    }

    //Event Chart
    public function event_chart()
    {
        try
        {
            // $present = Carbon::now();
            // $past = Carbon::now()->subMonth()->toDateString();

            $events = DB::table('event')->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as count'))
                            ->groupBy('date')->get();
            $event = [];
            $dateLabel = [];
            // $application = [1,3,7,2,8,5];

            // $dateLabel = ["2019-01-10","2019-01-12","2019-01-15","2019-01-18","2019-01-20","2019-01-23"];

            foreach($events as $value)
            {
                array_push($dateLabel,$value->date);
                array_push($event,$value->count);
            }

            $response = [
                'msg' => 'Total Events',
                'status' => 1,
                'events' => $event,
                'dateLabel' => $dateLabel
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }

        return response()->json($response);
    }

    //Busisness Chart
    public function business_chart()
    {
        try
        {
            // $present = Carbon::now();
            // $past = Carbon::now()->subMonth()->toDateString();

            $business_user = DB::table('business')->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as count'))
                            ->groupBy('date')->get();
            $business = [];
            $dateLabel = [];
            // $application = [1,3,7,2,8,5];

            // $dateLabel = ["2019-01-10","2019-01-12","2019-01-15","2019-01-18","2019-01-20","2019-01-23"];

            foreach($business_user as $value)
            {
                array_push($dateLabel,$value->date);
                array_push($business,$value->count);
            }

            $response = [
                'msg' => 'Total Business',
                'status' => 1,
                'business' => $business,
                'dateLabel' => $dateLabel
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }

        return response()->json($response);
    }

    public function user_chart()
    {
        try
        {
            // $present = Carbon::now();
            // $past = Carbon::now()->subMonth()->toDateString();

            $users = DB::table('user_registration')->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as count'))
                            ->groupBy('date')->get();
            $user = [];
            $dateLabel = [];
            // $application = [1,3,7,2,8,5];

            // $dateLabel = ["2019-01-10","2019-01-12","2019-01-15","2019-01-18","2019-01-20","2019-01-23"];

            foreach($users as $value)
            {
                array_push($dateLabel,$value->date);
                array_push($user,$value->count);
            }

            $response = [
                'msg' => 'Total Business',
                'status' => 1,
                'user' => $user,
                'dateLabel' => $dateLabel
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }

        return response()->json($response);
    }
}
