<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_Registration;
use Validator;
use Mail;
use App\Mail\UserMail;
use Hash;
use DB;

class UserController extends Controller
{
    public function registration(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required',
                'password' => 'required',
                'pincode' => 'required',
                'gender' => 'required',
                'dob' => 'required',
                'user_type' => 'required',
                'address' => 'required'
            ]);

            if($validator->fails())
            {   
                $response = [
                    'msg' => $validator->errors()->all(),
                    'status' => 0
                ];
            }
            else
            {
                $user = DB::table('user_registration')->where('email',$request->email)->first();
                 //return response()->JSON($user);
                if($user)
                {
                    $response = [
                        'msg' => 'Email Already Exits.Please Procced with login.',
                        'status' => 0,
                    ];
                }
                else
                {
                    $user = new  User_Registration;
                    $user->fname = $request->fname;
                    $user->lname = $request->lname;
                    $user->email = $request->email;
                    $user->password = Hash::make($request->password);
                    $user->phone_no = $request->phone_no;
                    $user->gender = $request->gender;
                    $user->dob = $request->dob;
                    $user->user_type = $request->user_type;
                    $user->address = $request->address;
                    $user->pincode = $request->pincode;
                    $user->save();

                    if($user->save())
                    {
                        $code = rand(1000,9999);
                        $data = [
                           'code' => $code,
                           'fname' => $user->fname
                        ];
        
                        Mail::to($request->email)->send(new UserMail($data));
                        
                        $response = [
                            'code' => $code,
                            'user_id' => $user->user_id,
                            'msg' => 'Successful User Registerd',
                            'status' => 1,
                        ];
                    }
                }
                
            }
        }
        catch(\Exception $e)
        {   
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0,
            ];
        }
        return response()->JSON($response);
    }

    function logout(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_id' => 'required',
        ]);

        if($validator->fails())
        {   
            $response = [
                'msg' => $validator->errors()->all(),
                'status' => 0
            ];
        }
        else
        {
            $user = User_Registration::find($request->user_id);
            $user->fcm_id = '';
            $user->save();
            return ['status' => 1, 'msg' => 'Logout Successfully.'];
        }
    }
}
