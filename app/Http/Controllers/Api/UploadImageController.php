<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\UploadImage;

class UploadImageController extends Controller
{
    public function upload(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'images' => 'required'
            ]);

            if ($request->hasFile('images')) {
                $images = UploadImage::Image($request, 'images');
                $image_path = [];
                $path = config('CustomVariable.image_dir');
                foreach ($images as $value) {
                    $temp = url('/') . $path . $value;
                    array_push($image_path, $temp);
                }

                $response = [
                    'msg' => 'Image Successfully Uploaded',
                    'data' => $image_path,
                    'status' => 1
                ];
            } else {
                $response = [
                    'msg' => 'Cant Image Upload ',
                    'status' => 0
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'msg' => $e->getMessage() . " " . $e->getFile() . " " . $e->getLine(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }
}
