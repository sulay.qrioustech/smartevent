<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EventCreate;
use Carbon;

class EventListController extends Controller
{
    public function upcommingEvent()
    {
        try
        {
            $mytime = Carbon\Carbon::now();
            $date = $mytime->toDateString();
            $event = EventCreate::Where('start_date','>=',$mytime->toDateString())->where('event_status',1)->get();

           
            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($event as $value)
            {
                $image_path =[];
                //return $value->logo_image;
                $value->logo_image= url('/').$path.$value->logo_image;    
                $imagess = json_decode($value->images);
                //return $imagess;
                foreach($imagess as $value)
                 {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $event[$i]['images'] = $image_path;
                $i++;
            }  

            if($event)
            {
                $response = [
                    'msg' => 'successfull upcomming  event transfer',
                    'data' => $event,
                    'status' => 1
                ];
            }
            else{
                $response = [
                    'msg' => 'Cant find upcomming event',
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function pastEvent()
    {
        try
        {
            $mytime = Carbon\Carbon::now();
            $date = $mytime->toDateString();
            $event = EventCreate::Where('start_date','<',$mytime->toDateString())->where('event_status',1)->get();

            
            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($event as $value)
            {
                $image_path =[];
                //return $value->logo_image;
                $value->logo_image= url('/').$path.$value->logo_image;    
                $imagess = json_decode($value->images);
                //return $imagess;
                foreach($imagess as $value)
                 {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $event[$i]['images'] = $image_path;
                $i++;
            }  


            if($event)
            {
                $response = [
                    'msg' => 'successfull past event transfer',
                    'data' => $event,
                    'status' => 1
                ];
            }
            else{
                $response = [
                    'msg' => 'Cant find upcomming event',
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
}
