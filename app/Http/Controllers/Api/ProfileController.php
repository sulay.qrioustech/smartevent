<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_Registration;

class ProfileController extends Controller
{
    public function profile(Request $request)
    {
        try
        {
            $user_id = $request->user_id;
            $user = User_Registration :: find($user_id);

            if($user)
            {
                $data = [
                    'fname' => $user->fname,
                    'lname' => $user->lname,
                    'email' => $user->email,
                    'phone_no' => $user->phone_no,
                    'address' => $user->address,
                ];

                $response = [
                    'msg' => 'successfull find profile',
                    'data' => $data,
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'Cant find user_id',
                    'status' =>0  
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' =>0  
            ];
        }
        return response()->JSON($response);
    }

    public function editProfile(Request $request)
    {
        try
        {
            //return $request->user_id;
            $user_id = $request->user_id;
            $user = User_Registration::find($user_id);

            if($user)
            {
                $user->fname = $request->fname;
                $user->email = $request->email;
                $user->lname = $request->lname;
                $user->phone_no = $request->phone_no;
                $user->address = $request->address;
                //$user->password = Hash::make($request->password);
                $user->save();

                if($user->save())
                {
                    $response = [
                        'msg' => 'successfully profile updated.',
                        'status' => 1
                    ];
                }
                else
                {
                    $response = [
                        'msg' => "can't edit profile.",
                        'status' => 0
                    ];
                }
            }
            else
            {
                $response = [
                    'msg' => 'cant find user_id',
                    'status' => 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' =>0  
            ];
        }
        return response()->JSON($response);
    }
}
