<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AboutUs;

class AboutController extends Controller
{
    public function about()
    {
        try     
        {
            $about = AboutUs::find(1);
            
            $data = [
                'description' => preg_replace( "/\r|\n/", "", $about->description),
            ];

            $response = [
                'msg' => 'Successfull data',
                'status' => 1,
                'data' => $data
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getLine()." ".$e->getFile()." ".$e->getMessage(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
}
