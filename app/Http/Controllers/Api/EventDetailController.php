<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\EventCreate;
use App\BusinessCreate;
use App\event_business;

class EventDetailController extends Controller
{
    public function getEventDetail(Request $request)
    {
        try
        {   
            $event_id = $request->event_id;
            $mainQuery = event_business::with('business')->where('event_id',$event_id)->where('business_approve_status','=',1);
            $event_business = $mainQuery->get();
            $business_count = $mainQuery->count();
            //return $event_business;
            //event image   
            $image_path =[];
            $path = config('CustomVariable.image_dir');

            $event = EventCreate::find($event_id);
            $event->logo_image= url('/').$path.$event->logo_image;    
            $imagess = json_decode($event->images);
            //return $imagess;
            foreach($imagess as $value)
            {
                $temp = url('/').$path.$value;
                array_push($image_path,$temp);
            }
            // $event['company_name'] = $event_business[0]->business->company_name;
            $event['images'] = $image_path;
            $event['whatsapp'] = $event->whatsapp == NULL ? "" : $event->whatsapp;
            $event['instagram'] = $event->instagram== NULL ? "" : $event->instagram; 
            $event['facebook'] = $event->facebook== NULL ? "" : $event->facebook;


            //business image
            $path = config('CustomVariable.image_dir');
            $i = 0;
            foreach($event_business as $value)
            {
                $image_path =[];
                $value->business->logo_image= url('/').$path.$value->business->logo_image;    
                $imagess = json_decode($value->business->images);
                //return $imagess;
                foreach($imagess as $value)
                {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $event_business[$i]['business']['images'] = $image_path;
                $i++;
            }
            $business= BusinessCreate::find($request->business_id);
            //return $business;
            if($business)
            {
                $response = [
                    'msg' => 'successfull data transfer',
                    'data' => $event,
                    'business' => $event_business,
                    'business_count' => $business_count,
                    'business_staus' => $business->business_status,
                    'status' => 1
                ];
    
            }
            else
            {
                $response = [
                    'msg' => 'successfull data transfer',
                    'data' => $event,
                    'business' => $event_business,
                    'business_count' => $business_count,
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function getEventDetailClone(Request $request)
    {
        try
        {   
            $event_id = $request->event_id;
            $mainQuery = event_business::with(['business','events'])->where('event_id',$event_id);
            $event_business = $mainQuery->get();
            $business_count = $mainQuery->count();

            return $event_business;

            //event image
            $image_path =[];
            $path = config('CustomVariable.image_dir');

            $event = EventCreate::find($event_id);
            $event->logo_image= url('/').$path.$event->logo_image;    
            $imagess = json_decode($event->images);
            //return $imagess;
            foreach($imagess as $value)
            {
                $temp = url('/').$path.$value;
                array_push($image_path,$temp);
            }
            $event['images'] = $image_path;
           


            //business image
            
            $path = config('CustomVariable.image_dir');
            $i = 0;
            foreach($event_business as $value)
            {
                $image_path =[];
                $value->business->logo_image= url('/').$path.$value->business->logo_image;    
                $imagess = json_decode($value->business->images);
                //return $imagess;
                foreach($imagess as $value)
                {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $event_business[$i]['business']['images'] = $image_path;
                $i++;
            }
        
            $response = [
                'msg' => 'successfull data transfer',
                'data' => $event,
                'business' => $event_business,
                'business_count' => $business_count,
                'status' => 1
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
}
