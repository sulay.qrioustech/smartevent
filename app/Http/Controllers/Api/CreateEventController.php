<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\EventCreate;
use Mail;
use App\User_Registration;
use App\Mail\EventMail;
use App\UploadImage;

class CreateEventController extends Controller
{
    public function event(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(),[
                'event_name' => 'required',
                'description' => 'required',
                'event_organizar_name' => 'required',
                'start_date' => 'required',
                'start_time' => 'required',
                'end_date' => 'required',
                'end_time' => 'required',
                'address' => 'required',
                'phone_number'=>'required',
                'logo_image' => 'required',
                'images' => 'required',
                'user_id' => 'required',
            ]);

            if($validator->fails())
            {
                $response = [
                    'msg' => 'Every Field Is Required',
                    'status' => 0
                ];
            }
            else
            {  
                $images_name = $request->images;
                $tempArr = [];
                foreach($images_name as $value)
                {
                    //\Log::info($value);
                    $no=strrpos($value,'/');
                    $str = substr($value,$no+1);
                    array_push($tempArr,$str);   
                }
            

                $logo_images = $request->logo_image;
                $no=strrpos($logo_images,'/');
                $str = substr($logo_images,$no+1);
                //return $str;

                $event = new EventCreate;
                $event->event_name = $request->event_name;
                $event->description = $request->description;
                $event->event_organizar_name = $request->event_organizar_name;
                $event->start_date = $request->start_date;
                $event->end_date = $request->end_date;
                $event->address = $request->address;
                $event->phone_number = $request->phone_number;
                $event->logo_image = $str;
                $event->images = json_encode($tempArr);
                $event->user_id = $request->user_id;
                $event->start_time = $request->start_time;
                $event->end_time = $request->end_time;
                $event->whatsapp = $request->whatsapp;
                $event->instagram = $request->instagram;
                $event->facebook = $request->facebook;
                $event->save();
                 
                if($event->save())
                {
                    $user = User_Registration::find($event->user_id);
                    
                    $useremail=$user->email;
                    
                    //Mail::to($useremail)->send(new EventMail());
                    $event_status1 = EventCreate::find($event->event_id);
                    $response = [
                        'event_id' => $event->event_id,
                        'approve_status' => $event_status1->event_status,
                        'msg' => 'Your event has been registered successfully. The administrator will approve soon.',
                        'status' => 1
                    ];
                }
                else
                {
                    $response = [
                        
                        'msg' => "Can't Store Data Successfully",
                        'status' => 0
                    ];
                }
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ". $e->getLine()." ". $e->getFile(),
                'status' => 0   
            ];
        }
        return response()->json($response);
    }
    
    public function createevent(Request $request)
    {
        try
        {
            $validator = Validator::make($request->all(),[
                'event_name' => 'required',
                'description' => 'required',
                'event_organizar_name' => 'required',
                'start_date' => 'required',
                'start_time' => 'required',
                'end_date' => 'required',
                'end_time' => 'required',
                'address' => 'required',
                'phone_number'=>'required',
                'logo_image' => 'required',
                'images' => 'required',
                'user_id' => 'required',
            ]);

            if($validator->fails())
            {
                $response = [
                    'msg' => $validator->errors()->all(),
                    'status' => 0
                ];
            }
            else
            {  
                
                if($request->hasFile('images'))
                { 
                    $images = UploadImage::Image($request,'images');
                }   
                else
                {
                    $images = ["noimage.jpg"];
                }
                
                if($request->hasFile('logo_image'))
                { 
                    $logo_image = UploadImage::Image1($request,'logo_image');
                }   
                else
                {
                    $logo_image = "noimage.jpg";
                }

                $event = new EventCreate;
                $event->event_name = $request->event_name;
                $event->description = $request->description;
                $event->event_organizar_name = $request->event_organizar_name;
                $event->start_date = $request->start_date;
                $event->end_date = $request->end_date;
                $event->address = $request->address;
                $event->phone_number = $request->phone_number;
                $event->logo_image = ($logo_image);
                $event->images = json_encode($images);
                $event->user_id = $request->user_id;    
                $event->start_time = $request->start_time;
                $event->end_time = $request->end_time;
                $event->save();
                
                if($event->save())
                {
                    $user = User_Registration::find($event->user_id);
                    
                    $useremail=$user->email;
                    
                    $event_status1 = EventCreate::find($event->event_id);
                    $response = [
                        'event_id' => $event->event_id,
                        'approve_status' => $event_status1->event_status,
                        'msg' => 'your event is registered successfully. admin will approve soon.',
                        'status' => 1
                    ];
                }
                else
                {
                    $response = [
                        
                        'msg' => 'Cant Store Data Successfully',
                        'status' => 0
                    ];
                }
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ". $e->getLine()." ". $e->getFile(),
                'status' => 0   
            ];
        }
        return response()->json($response);
    }

    public function searchEventList()
    {
        try{
            $event = EventCreate::where('event_status',1)->get();  
            $data = [];             
            $path = config('CustomVariable.image_dir');
            foreach($event as $value)
            {   
                //return $value->logo_image;
                $value->logo_image= url('/').$path.$value->logo_image;    
    
                $temp = [
                    'event_id' => $value->event_id,
                    'event_name' => $value->event_name,
                    'event_image' => $value->logo_image,
                ];

                array_push($data,$temp);
            }
            
            if($event)
            {
                $response = [
                    'event' => $data,
                    'msg' => 'Succesfull search event',
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'Event doesnot exist',
                    'status' => 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getLine()." ".$e->getMessage()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }

    public function shareEvent()
    {
        try{
            $response = [
                'msg' => 'Static Share Event',
                'data' => 'http://sultanimakutano.surge.sh/singleEvent.html',
                'status' => 1
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg'=> $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }
}
