<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Login;
use Validator;
use Auth;
use DB;
use Hash;
use App\User_Registration;
use Mail;
use App\Mail\UserMail;   
use App\BusinessCreate;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        try{
            $validator = Validator :: make($request->all(),[
                'email' => 'required',
                'password' => 'required',
                'fcm_id' => 'required'
            ]);

            if($validator->fails())
            {
                $response = [
                    'msg' => 'Please fill all the details',
                    'status' => 0
                ];
            }
            else
            {
                $user = DB::table('user_registration')->where('email',$request->email)->first();
                if($user)
                {   
                    if($user->verification_type == 1)
                    {
                        if($user->user_type == 1)
                        {
                            if(Hash::check($request->password, $user->password)) 
                            {
                                $users = User_Registration::where('email',$request->email)->increment('login_count');
                                $user = User_Registration::select('user_id')->where('email',$request->email)->get();
                                $user_count = User_Registration::select('login_count','login_status','user_type','user_id')->find($user);
                                
                                $response = [
                                    'login_count' => $user_count,
                                    'msg' => 'Successful Login',
                                    'status' => 1
                                ];
                            }
                            else
                            {
                                $response = [
                                    'msg' => 'Invalid Password',
                                    'status' => 0,
                                ];
                            }
                        }
                        else if($user->user_type == 2)
                        {
                            if(Hash::check($request->password, $user->password)) 
                            {   
                              
                                $user_fcm = User_Registration::where('email',$request->email)->first();
                               
                                $user_fcm->fcm_id = $request->fcm_id;
                                $user_fcm->save();
                                //return 'hii';
                                $users = User_Registration::where('email',$request->email)->increment('login_count');
                                $user = User_Registration::select('user_id')->where('email',$request->email)->first();
                                //return $user;
                                $user_count = User_Registration::select('login_count','login_status','user_type','user_id')->find($user);
                                // $business = BusinessCreate::find($user->user_id);
                                $business = BusinessCreate::where('user_id',$user->user_id)->first();
                                //return $business;
                                if($business)
                                {
                                    $response = [
                                        'business_id' => $business->business_id,
                                        'login_count' => $user_count,
                                        'msg' => 'Successful Login',
                                        'status' => 1
                                    ];
                                }
                                else
                                {
                                    $response = [
                                        'login_count' => $user_count,
                                        'msg' => 'Successful Login',
                                        'status' => 1
                                    ];
                                }
                                
                            }
                            else
                            {
                                $response = [
                                    'msg' => 'Invalid Password',
                                    'status' => 0,
                                ];
                            }
                        }
                        else{
                            if(Hash::check($request->password, $user->password)) {
                                $user_fcm = User_Registration::where('email',$request->email)->first();
                                $user_fcm->fcm_id = $request->fcm_id;
                                $user_fcm->save();

                                $users = User_Registration::where('email',$request->email)->increment('login_count');
                                $user = User_Registration::select('user_id')->where('email',$request->email)->get();
                                $user_count = User_Registration::select('login_count','login_status','user_type','user_id')->find($user);
                                
                                $response = [
                                    'login_count' => $user_count,
                                    'msg' => 'Successful Login',
                                    'status' => 1
                                ];
                            }
                            else
                            {
                                $response = [
                                    'msg' => 'Invalid Password',
                                    'status' => 0,
                                ];
                            }
                        }
                    }
                    else
                    {
                        $code = rand(1000,9999);
                        $data = [
                            'fname' => '',
                            'code' => $code
                        ];
        
                        Mail::to($request->email)->send(new UserMail($data));
                        $user_id=$user->user_id;
                        $response = [
                            'code' => $code,
                            'user_id' => $user_id,
                            'msg' => 'Please verify your email address to proceed',
                            'status' => 2
                        ];
                    }
                }
                else
                {
                    
                    $response = [
                        'msg' => "This email does not exists.",
                        'status' => 0,
                    ];
                }
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getFile()." ".$e->getLine()." ".$e->getMessage(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
}   




