<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_Registration;
use App\Mail\forgetpassword;
use Hash;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordController extends Controller
{
    public function forgetpassword(Request $request)
    {
        $email = $request->email;
        try {
            $login = User_Registration::where(['email' => $email])->first();
            $code = rand();
            if ($login) {
                $data = [
                    'code' => $code
                ];

                $login->password = Hash::make($code);
                $login->save();

                mail::to($email)->send(new forgetpassword($data));

                $response = [
                    "msg" => "The new password will be sent to your email.",
                    "status" => 1
                ];
            } else {
                $response = [
                    "msg" => "Email doesn't exist. Please create an account.",
                    "status" => 0
                ];
            }
        } catch (\Exception $e) {
            $response = [
                "msg" => $e->getMessage() . " " . $e->getFile() . " " . $e->getLine(),
                "status" => 0
            ];
        }
        return response()->JSON($response);
    }
}
