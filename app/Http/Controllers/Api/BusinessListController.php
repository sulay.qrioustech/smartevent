<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusinessCreate;
use App\User_Registration;
use App\Favourite;
use App\EventCreate;
use App\event_business;

class BusinessListController extends Controller
{
    public function getBusinessList(Request $request)
    {
        try
        {
            $business = BusinessCreate::with(['user','user_favourite' => function($query) use($request) {
                return $query->where('user_id',$request->user_id);
            }])->where('business_status',1)->get();

            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($business as $value)
            {
                $image_path =[];
                //return $value->logo_image;
                $value->logo_image= url('/').$path.$value->logo_image;    
                $imagess = json_decode($value->images);
                //return $imagess;
                foreach($imagess as $value)
                {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $business[$i]['images'] = $image_path;
                $i++;
            }  

            if($business)
            {
                $response = [
                    'msg' => 'successfull transfer business list',
                    'data' => $business,
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'cant approve business by admin',
                    'status'=> 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
    
    public function singleBusinessDetail(Request $request)
    {
        try
        {  
            $business_id = $request->business_id;
            $business = BusinessCreate::with('user')->find($business_id);
            
            $image_path =[];
            $path = config('CustomVariable.image_dir');
            $business->logo_image = url('/').$path.$business->logo_image;    
            
            $imagess = json_decode($business->images);
            //return $imagess;
            foreach($imagess as $value)
                {
                $temp = url('/').$path.$value;
                array_push($image_path,$temp);
            }
            $business['images'] = $image_path;
               
            
            if($business)
            {
                $response = [
                    'msg' => 'Successfull businessdetail transfer',
                    'data' => $business,
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'Business_id doesnot exist',
                    'status' => 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];   
        }
        return response()->json($response);
    }

    public function selectEvent(Request $request)
    {
        try
        {
            $event_id = $request->event_id;
            $business_id = $request->business_id;
            //$event = EventCreate::find($event_id);
            $event = event_business::where(['business_id' => $request->business_id ,'event_id' =>$request->event_id ])->first();

            if($event)
            {
                $response = [
                    'msg' => 'Oops the business already registered',
                    'status' => 0
                ];
            }
            else
            {
                $business = BusinessCreate::find($request->business_id);
                if($business->business_status == 1)
                {
                    $event_business = new event_business;
                    $event_business->event_id = $event_id;
                    $event_business->business_id = $business_id;
                    $event_business->save();
    
                    if($event_business->save())
                    {
                        $event =  event_business::find($event_business->event_business_id);
                        $response = [
                            'business_approve_status' => $event->event_status,
                            'msg' => 'successfull registerd in event',
                            'status' => 1
                        ];
                    }
                    else
                    {
                        $response = [
                            'msg' => 'cant registered in event',
                            'status' => 1
                        ];
                    }
                }
                else
                {
                    $response = [
                        'msg' => 'Still Your business is not approved by Admin',
                        'status' => 0
                    ];
                } 
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function selectEventList(Request $request)
    {
        try
        {   
            // $business_id = $request->business_id;
            // $event = BusinessCreate::with(['events' => function($query){
            //     return $query->where('event_status','=',1);
            // }])->find($business_id);
        
            $event = event_business::with('events')->where('business_id',$request->business_id)->get();
            //return $event;
            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($event as $value)
            {
                $image_path =[];
                //return $value->logo_image;
                $value->events->logo_image= url('/').$path.$value->events->logo_image;    
                $imagess = json_decode($value->events->images);
                foreach($imagess as $value)
                {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $event[$i]['events']['images'] = $image_path;
                $i++;
            }  

            if(count($event) > 0)
            {
                $response = [
                    'event_list' => $event,
                    'msg' => 'successfull events',
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg'=>'cant find event',
                    'status'=> 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }

    public function favouriteBusiness(Request $request)
    {
        try
        {
            //return json_encode($request->business_id);
            $user = $request->user_id;
            $business = $request->business_id;
            $Businsess = BusinessCreate::where('business_id',$business)->first();
            if($Businsess)
            {
                $favourite = Favourite::where(['business_id' => $request->business_id ,'user_id' =>$request->user_id ])->first();
                if($favourite)
                {
                    $response = [
                        'msg' => "Can't add favourite",
                        'status' => 0
                    ];   
                }
                else
                {
                    $favourite = new Favourite;
                    $favourite->user_id = $request->user_id;
                    $favourite->business_id = $request->business_id;
                    $favourite->save();
                    
                    if($favourite->save())
                    {
                        $response = [
                            'favourite_id' => $favourite->favourite_id,
                            'msg' => 'Successfully added to favourite list',
                            'status' => 1
                        ]; 
                    }
                    else
                    {
                        $response = [
                            'msg' => "Can't add favourite",
                            'status' => 0
                        ];   
                    }
                }
            }
            else
            {
                $response = [
                    'msg' => 'Cant find Business.',
                    'status' => 0
                ];
            }   
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function getfavouriteBusineess(Request $request)
    {
        try
        {
            $business_id = Favourite::with(['business'=>function($query){
                return $query->where(['business_status'=>1]);
            },'business.user'])->where('user_id',$request->user_id)->get();
            
            //return $business_id;
            $path = config('CustomVariable.image_dir');
            $i=0;
            foreach($business_id as $value)
            {
                $image_path =[];
                //return $value->logo_image;
                $value->business->logo_image= url('/').$path.$value->business->logo_image;    
                $imagess = json_decode($value->business->images);
                //return $imagess;
                foreach($imagess as $value)
                {
                    $temp = url('/').$path.$value;
                    array_push($image_path,$temp);
                }
                $business_id[$i]['business']['images'] = $image_path;
                $i++;
            }
            if(count($business_id) > 0)
            {   
                $response = [
                    'data' => $business_id,    
                    'msg' => 'Successfull find list',
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'Cant find business list',
                    'status' => 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function deletefavourites(Request $request)
    {
        try
        {
            $favourite_id = $request->favourite_id;

            
            $favourite = Favourite::find($favourite_id);
            if($favourite)
            {
                $favourite->delete();    
                $response = [
                    'msg' => 'Successfull Unfavourite',
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => 'Cant find favourite id',
                    'status' => 0
                ];
            }
        
         
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getFile()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }
}
