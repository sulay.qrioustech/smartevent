<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Chat;
use Validator;
use App\User_Registration;
use App\BusinessCreate;

class ChatController extends Controller
{
    public function chat(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'chat_id' => 'required',
                'from' => 'required',
                'to' => 'required',
                'message' => 'required',
                'time' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'msg' => $validator->errors()->all(),
                    'status' => 0
                ];
            } else {
                $chat = new Chat();
                $chat->chat_id = $request->chat_id;
                $chat->from = $request->from;
                $chat->to = $request->to;
                $chat->message = $request->message;
                $chat->time = $request->time;
                $chat->save();

                $to = User_Registration::select('fcm_id')->where('user_id', $request->to)->first();

                $from = User_Registration::where('user_id', $request->to)->first();
                $sender_data =  User_Registration::where('user_id', $request->from)->first();
                $name = $sender_data->fname . ' ' . $sender_data->lname;
                $key = 'AIzaSyAxhMQ3FAjwc30_dwYAIcNuu7nCmGukBbg';
                $url = 'https://fcm.googleapis.com/fcm/send';

                $fields = '{
                    "to": "' . $to->fcm_id . '",
                    "data": {
                        "body":"' . $request->message . '",
                        "title":"' . $name . '",
                        "chat_id":"' . $request->chat_id . '"
                    },
                    "notification":{
                        "body":"' . $request->message . '",
                        "title":"' . $name . '",
                        "chat_id":"' . $request->chat_id . '",
                        "sound":"default"
                    },
                }';

                $headers = [
                    'Authorization: key=' . $key,
                    'Content-Type: application/json'
                ];

                $ch = curl_init();
                //print_r(json_encode($fields)); exit;
                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);

                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

                // Execute post
                $result = curl_exec($ch);
                //echo $result;
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }

                // Close connection
                curl_close($ch);

                $chat_info = Chat::find($chat->id);
                //return $chat_info; 
                if ($chat->save()) {
                    $response = [
                        'from' => $from,
                        'to' => $to,
                        'chat' => $chat_info,
                        'msg' => 'Successfull Chat Send',
                        'status' => 1
                    ];
                } else {
                    $response = [
                        'msg' => 'Cant Chat Send',
                        'status' => 0
                    ];
                }
            }
        } catch (\Exception $e) {
            $response = [
                'msg' => $e->getFile() . " " . $e->getLine() . " " . $e->getMessage(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }

    public function getchat(Request $request)
    {
        try {
            $chat_info = Chat::where('chat_id', $request->chat_id)->get();

            if (count($chat_info)) {
                $response = [
                    'chat' => $chat_info,
                    'msg' => 'Chat Found',
                    'status' => 1
                ];
            } else {
                $response = [
                    'msg' => 'Chat not found',
                    'status' => 0
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'msg' => $e->getFile() . " " . $e->getLine() . " " . $e->getMessage(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }

    public function get_chat_user(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'user_type' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'msg' => $validator->errors()->all(),
                    'status' => 0
                ];
            } else {
                if ($request->user_type == 1) {
                    return ['status' => 0, 'msg' => 'No User Found.'];
                } else {
                    $users = Chat::distinct()->select('chat_id')->where('from', '=', $request->user_id)
                        ->orWhere('to', '=', $request->user_id)->get();
                }
                $results = [];
                $j = 0;
                if (count($users) > 0) {
                    foreach ($users as $user) {
                        $message_data = Chat::where('chat_id', $user->chat_id)->orderBy('id', 'DESC')->first();

                        if ($request->user_type == 3) {
                            $temp = explode("_", $user->chat_id);
                            $user_data = BusinessCreate::where('user_id', $temp[1])->first();
                            //$result['info'] = $user_data;
                            $result['user_id'] = $user_data->business_id;
                            $result['name'] = $user_data->company_name;
                            $result['chat_id'] = $user->chat_id;
                            $result['last_message'] = $message_data->message;
                            $result['time'] = $message_data->time;
                            $result['image'] = url('/') . '/storage/images/' . $user_data->logo_image;
                            array_push($results, $result);
                        } else {
                            if ($request->user_type == 2) {
                                $temp = explode("_", $user->chat_id);
                                $user_data = User_Registration::find($temp[2]);

                                $result['user_id'] = $user_data->user_id;
                                $result['name'] = $user_data->fname . ' ' . $user_data->lname;
                                $result['chat_id'] = $user->chat_id;
                                $result['last_message'] = $message_data->message;
                                $result['time'] = $message_data->time;
                                $result['image'] = '';
                                array_push($results, $result);
                            }
                        }
                    }
                } else {
                    return ['status' => 0, 'msg' => 'No User Found.'];
                }

                return ['status' => 1, 'data' => $results, 'msg' => 'success'];
            }
        } catch (\Exception $e) {
            $response = [
                'msg' => $e->getMessage(),
                'status' => 0
            ];
        }
    }
}
