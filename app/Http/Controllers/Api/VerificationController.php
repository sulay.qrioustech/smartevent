<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_Registration;

class VerificationController extends Controller
{
    public function verification(Request $request)
    {
       // return $request->user_id;
        try
        {
            $user = User_Registration::where('user_id',$request->user_id)->first();
           // return $user;
            $user->verification_type = 1;
            $user->save();

            $response = [
                'msg' => 'Successfull Verified User',
                'status' => 1
            ];
        }
        catch(\Exception $e)
        {
            $response = [
               'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
               'status' => 0        
            ];
        }
        return response()->JSON($response);
    }
}
