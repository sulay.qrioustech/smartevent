<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use Validator;
use App\Mail\ContactMail;  

class ContactController extends Controller
{
    public function insert(Request $request)   
    {
        try{
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'email'=>'required | email',
                'message'=>'required',
                'phone_number' => 'required'
            ]);

            if($validator->fails())
            {
                $response = [
                    'msg' => $validator->errors()->all(),
                    'status' => 0
                ];
            }
            else
            {
                $data  = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'message' => $request->message,
                    'phone_number' => $request->phone_number
                ];
                      
                $event = new Contact;
                $event->name = $request->name;
                $event->email = $request->email;
                $event->message = $request->message; 
                $event->phone_number = $request->phone_number;
                $event->save();

                //Mail::to($request->email)->send(new EventMail($data));

                $response = [
                    'msg' => 'Contact Registration Successful.',
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }   
        return response()->JSON($response);
    }   
}
