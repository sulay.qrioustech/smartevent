<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BusinessCreate;
use Validator;
use Mail;
use App\User_Registration;
use App\Mail\BusinessMail;
use App\UploadImage;

class CreateBusinessController extends Controller
{
    public function business(Request $request)
    {
        
        $validator = Validator::make($request->all(),[
            'company_name' => 'required',
            'description' => 'required',
            'logo_image' => 'required',
            'images' => 'required',
            'user_id' => 'required'
        ]);

        if($validator->fails())
        {
            $response = [
                'msg' => 'Every Field Is Required',
                'status' => 0
            ];
        }
        else
        {
            $images_name = $request->images;
            $tempArr = [];
            foreach($images_name as $value)
            {
                //\Log::info($value);
                $no=strrpos($value,'/');
                $str = substr($value,$no+1);
                array_push($tempArr,$str);   
            }
           
            $logo_imagess = $request->logo_image;
            $no=strrpos($logo_imagess,'/');
            $str = substr($logo_imagess,$no+1);
            //return $str;

            $business = new BusinessCreate;
            $business->company_name = $request->company_name;
            $business->description = $request->description;
            $business->logo_image = $str;
            $business->images = json_encode($tempArr);
            $business->user_id = $request->user_id;
            $business->facebook = $request->facebook;
            $business->instagram = $request->instagram;
            $business->whatsapp = $request->whatsapp;
            $business->save();

            if($business->save())
            {
                $user = User_Registration::find($business->user_id);
                $business_status1 = BusinessCreate::find($business->business_id);
                if($user->login_status == 1)
                {
                    $response = [
                        'business_id' => $business->business_id,
                        'approve_status' => $business_status1->business_status,
                        'msg' => 'Your business has been registered successfully. The administrator will approve soon.',
                        'status' => 1
                    ];
                }
                else{
                    $user->login_status = 1;
                    $user->save();

                    $response = [
                        'business_id' => $business->business_id,
                        'login_status' => $user->login_status,
                        'approve_status' => $business_status1->business_status,
                        'msg' => 'Your business has been registered successfully. The administrator will approve soon.',
                        'status' => 1
                    ];
                }
                //Mail::to($usermail)->send(new BusinessMail());
            }
            else
            {
                $response = [
                    'msg' => "Can't Create Business",
                    'status' => 0
                ]; 
            }
            
        }
        return response()->JSON($response);
    }

    public function createbusiness(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'company_name' => 'required',
            'description' => 'required',
            'logo_image' => 'required',
            'images' => 'required',
            'user_id' => 'required'
        ]);

        if($validator->fails())
        {
            $response = [
                'msg' => 'Every Field Is Required',
                'status' => 0
            ];
        }
        else
        {
            if($request->hasFile('images'))
            { 
                $images = UploadImage::Image($request,'images');
            }   
            else
            {
                $images = ["noimage.jpg"];
            }
            
            if($request->hasFile('logo_image'))
            { 
                $logo_image = UploadImage::Image1($request,'logo_image');
            }   
            else
            {
                $logo_image = "noimage.jpg";
            } 

            $business = BusinessCreate::where('user_id',$request->user_id)->first();
            //return $business;
            if($business)
            {
                $response = [
                    'msg' => 'You have Already created business',
                    'status' => 0
                ];
            }
            else
            {
                $business = new BusinessCreate;
                $business->company_name = $request->company_name;
                $business->description = $request->description;
                $business->logo_image = $logo_image;
                $business->images = json_encode($images);
                $business->user_id = $request->user_id;
                $business->save();

                if($business->save())
                {
                    $user = User_Registration::find($business->user_id);
                    $business_status1 = BusinessCreate::find($business->business_id);
                    if($user->login_status == 1)
                    {
                        $response = [
                            'business_id' => $business->business_id,
                            'approve_status' => $business_status1->business_status,
                            'msg' => 'your business is registered successfully. admin will approve soon.',
                            'status' => 1
                        ];
                    }
                    else{
                        $user->login_status = 1;
                        $user->save();

                        $response = [
                            'business_id' => $business->business_id,
                            'login_status' => $user->login_status,
                            'approve_status' => $business_status1->business_status,
                            'msg' => 'your business is registered successfully. admin will approve soon.',
                            'status' => 1
                        ];
                    }
                }
            // else
            // {
            //     $response = [
            //         'msg' => 'Cant Create Business',
            //         'status' => 0
            //     ]; 
            // }
           
            }
        
            return response()->JSON($response);
        }
    }

    public function getbusiness(Request $request)
    {
        try
        {
            $user_id = $request->user_id;
            $user = User_Registration::find($user_id);
            
            if($user)
            {
                $data =[
                    'name' => $user->fname,
                    'email' => $user->email,
                    'phone_number' => $user->phone_no,
                    'address' => $user->address 
                ];

                $response = [
                    'msg' => 'Data Transfer Successfull',
                    'data' => $data,
                    'status' => 1
                ];
            }
            else
            {
                $response = [
                    'msg' => "Can't Data Transfer Successfully",
                    'status' => 0
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getFile()." ".$e->getLine()." ".$e->getMessage(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function edit(Request $request)
    {
        try
        {
            $images_name = $request->images;
            $tempArr = [];
            foreach($images_name as $value)
            {
                //\Log::info($value);
                $no=strrpos($value,'/');
                $str = substr($value,$no+1);
                array_push($tempArr,$str);   
            }
        
            $logo_imagess = $request->logo_image;
            $no=strrpos($logo_imagess,'/');
            $str = substr($logo_imagess,$no+1);

            
            //return $request->all();
            $business = BusinessCreate::find($request->business_id);
            $business->company_name = $request->company_name;
            $business->description = $request->description;
            $business->logo_image = $str;
            $business->images =json_encode($tempArr);
            $business->user_id = $request->user_id;
            $business->business_status = 0;
            $business->whatsapp = $request->whatsapp == '' ? '' : $request->whatsapp;
            $business->instagram = $request->instagram== '' ? '' : $request->instagram;
            $business->facebook = $request->facebook== '' ? '' : $request->facebook;
            $business->save();

            if($business->save())
            {
                $user = User_Registration::find($request->user_id);
                $user->fname = $request->fname;
                $user->lname = $request->lname;
                $user->email = $request->email;
                $user->address = $request->address;
                $user->phone_no = $request->phone_no;
                $user->save();

                if($user->save())
                {
                    $response = [
                        'business_id'=>$business->business_id,
                        'msg'=> 'Your business is updated successfully. Administrator will approve soon.',
                        'status' => 1
                    ];
                }
                else
                {
                    $response = [
                        'msg' => 'can not edit business user detail.',
                        'status' =>0
                    ];
                }

            }
            else
            {
                $response = [
                    'msg' => 'can not edit business detail.',
                    'status' =>0
                ];   
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg'=>$e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }

    public function qrcode(Request $request)
    {
            $business_id = $request->business_id;

            return \QrCode::size($request->size)->generate($business_id);
    }

    public function getBusinessId(Request $request)
    {
        try{    
            $business_id = BusinessCreate::find($request->user_id);

            $response = [
                'data' => $business_id,
                'msg' => 'succesfull find business_id',
                'status' => 1
            ];

        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage." ".$e->getLine()." ".$e->getFile(),
                'status' => 1
            ];
        }
        return response()->json($response);
    }

    public function shareBusiness()
    {
        try{
            $response = [
                'msg' => 'Static Share Business',
                'data' => 'http://sultanimakutano.surge.sh/singleBusiness.html',
                'status' => 1
            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg'=> $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }
        return response()->json($response);
    }
}
