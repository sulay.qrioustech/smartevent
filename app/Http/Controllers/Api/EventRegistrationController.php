<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Event_Reg;
use App\Mail\EventMail;
use Mail;
use Hash;

class EventRegistrationController extends Controller
{
    public function registration(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'fname' => 'required',
                'lname' => 'required',
                'phone_no' => 'required',
                'gender' => 'required',
                'dob' => 'required',
                'email'=>'required | email',
                'password'=>'required'
            ]);

            if($validator->fails())
            {
                $response = [
                    'msg' => 'every field is required',
                    'status' => 0
                ];
            }
            else
            {
                $data  = [
                    'name' => $request->fname,
                ];
                      
                $event = new Event_Reg;
                $event->fname = $request->fname;
                $event->lname = $request->lname;
                $event->phone_no = $request->phone_no;
                $event->gender = $request->gender;
                $event->dob = $request->dob;
                $event->email = $request->email;
                $event->password = Hash::make($request->password);
                $event->save();

                Mail::to($request->email)->send(new EventMail($data));

                $response = [
                    'msg' => 'Registration Successfull',
                    'status' => 1
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getFile(),
                'status' => 0
            ];
        }   
        return response()->JSON($response);
    }
} 
