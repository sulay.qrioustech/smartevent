<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_Registration;
use App\BusinessCreate;

class BusinessDetailController extends Controller
{   
    public function business(Request $request)
    {
        try
        {
            $business_id = $request->business_id;
          //  return $business_id;
            $business = BusinessCreate::find($business_id);
            //return $business->user_id;
            $user_id = $business->user_id;
            $user_id = User_Registration::find($user_id);

            $data = [
                'fname' => $user_id->fname,
                'email' => $user_id->email,
                'phone_no' => $user_id->phone_no,
                'address' => $user_id->address
            ];

            $image_path =[];
            $path = config('CustomVariable.image_dir');
           
            $business->logo_image= url('/').$path.$business->logo_image;    
            $imagess = json_decode($business->images);
            //return $imagess;
            foreach($imagess as $value)
            {
                $temp = url('/').$path.$value;
                array_push($image_path,$temp);
            }
            $business['images'] = $image_path;

            $response = [
                'msg' => 'successfull transfer Data',
                'business' => $business,
                'data' => $data,
                'status' => 1

            ];
        }
        catch(\Exception $e)
        {
            $response = [
                'msg' => $e->getMessage()." ".$e->getLine()." ".$e->getLine(),
                'status' => 0
            ];
        }
        return response()->JSON($response);
    }   
}
