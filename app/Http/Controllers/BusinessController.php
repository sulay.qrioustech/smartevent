<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessCreate;
use App\User_Registration;
use Storage;
use App\Favourite;

class BusinessController extends Controller
{
    public function delete($id)
    {
        $business = BusinessCreate::find($id);
        $business->delete();
        Storage::delete('/storage/images/'.$business->logo_image);
        return redirect()->route('business');
    }

    public function view()
    {
        $business = BusinessCreate::all();
        return view('business/show')->with('business',$business);
    }

    public function businessOrganizarDetail($id)
    {
        $users = User_Registration::where('user_id',$id)->get();
        return view('business.businessUser')->with('users',$users);
    }

    public function favourite($id)
    {
        $user = Favourite::select('user_id')->where('business_id',$id)->get();
        $user = User_Registration::find($user);
        return view('business/favourite')->with('user',$user);
    }

    public function approve($id)
    {
        $business = BusinessCreate::find($id);
        $business->business_status = 1;
        $business->save();
        return redirect()->route('business');
    }

    public function disapprove($id)
    {
        $business = BusinessCreate::find($id);
        $business->business_status = 0;
        $business->save();
        return redirect()->route('business');
    }
} 



