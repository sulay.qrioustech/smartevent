<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_Registration;
use App\EventCreate;
use App\BusinessCreate;
use App\event_business;

class websiteController extends Controller
{
    public function index()
    {
        return view('website.index');
    }
    
    public function contact()
    {
        return view('website.contact');
    }

    public function business()
    {
        return view('website.businesslist');
    }

    public function events()
    {
        return view('website.event');
    }

    public function singleevent($id)
    {
        $event_id = [
            'id1' => $id
        ];
        return view('website.singleEvent')->with($event_id);
    }

    public function singlebusiness($id)
    {
        $business_id = [
            'id1' => $id
        ];
        return view('website.singleBusisness')->with($business_id);
    }

    public function selectEvent($id)
    {
        
        //return $id;
        $user_id = session('user_id');
        $business = BusinessCreate::select('business_id')->where('user_id',$user_id)->first();
        //return $business;
        //$event = EventCreate::find($event_id);
        $event = event_business::where(['business_id' => $business->business_id ,'event_id' =>$id ])->first();
        //return $event;
        if($event)
        {
            return view('website.event');
        }
        else
        {
            $business = BusinessCreate::find($request->business_id);
            if($business->business_status == 1)
            {
                $event_business = new event_business;
                $event_business->event_id = $event_id;
                $event_business->business_id = $business_id;
                $event_business->save();

                if($event_business->save())
                {
                    $event =  event_business::find($event_business->event_business_id);
                    $business_approve_status = $event->event_status;
                    // $response = [
                    //     'business_approve_status' => $event->event_status,
                    //     'msg' => 'successfull registerd in event',
                    //     'status' => 1
                    // ];
                    return view('website.event')->with('business_approve_status',$business_approve_status)->with('msg','successfull registerd in event');
                }
                else
                {
                    return view('website.event')->with('msg','successfull registerd in event');
                }
            }
        }
    }   

    public function favourite()
    {
        return view('website.favourite');
    }

    public function favouriteBusiness()
    {
        return view('website.favouriteBusiness');
    }
}
