<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    public $table = "about";

    protected $primaryKey = "about_id";

    public $timestamps = true;  
}
