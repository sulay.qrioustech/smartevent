<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table = "contact";

    protected $primaryKey = "contact_id";

    public $timestamps = true;
}
