<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{   
    public $table = "user_registration";

    protected $primaryKey = "user_id";

    public $timestamps = true;
}
