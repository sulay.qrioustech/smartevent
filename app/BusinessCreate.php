<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessCreate extends Model
{
    public $table = 'business';
    
    protected $primaryKey = 'business_id';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User_Registration','user_id');
    }

    public function user_favourite()
    {
        return $this->hasOne('App\Favourite','business_id');
    }

    public function events()    
    {
        return $this->hasMany('App\EventCreate','business_id');
    }


}
