@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; font-size:28px; margin-left:10px;">About Us</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($about as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td>{{$value->description}}
                                    <td><a href ="{{route('about-edit',['id' => $value->about_id])}}">Edit</a>/
                                        <a href ="">Delete</a>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection