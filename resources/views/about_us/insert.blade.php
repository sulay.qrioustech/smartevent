@extends('layouts/app')
@section('content')
    <div class="box-typical box-typical-padding">
        <h4 class="m-t-lg with-border">About Us</h4>
         <form method="post" action="{{route('about-insert')}}">
            {{csrf_field()}}
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Description</label>
                <div class="col-sm-10">
                   <textarea rows="4" class="form-control" name="description" required="true" placeholder="textarea"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="submit" class="btn btn-sm btn-inline btn-danger" value="Submit"/>
                </div>
            </div>  
            {{-- <div class="form-group row">
                <label class="col-sm-2 form-control-label">Text Disabled</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><input type="text" disabled class="form-control" id="inputPassword" placeholder="Text Disabled"></p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label">Text Readonly</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><input type="text" readonly class="form-control" id="inputPassword" placeholder="Text Readonly"></p>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 form-control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                </div>
            </div>
            <div class="form-group row">
                <label for="exampleSelect" class="col-sm-2 form-control-label">Select</label>
                <div class="col-sm-10">
                    <select id="exampleSelect" class="form-control">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="exampleSelect2" class="col-sm-2 form-control-label">Multiple select</label>
                <div class="col-sm-10">
                    <select multiple class="form-control" id="exampleSelect2">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="exampleSelect" class="col-sm-2 form-control-label">Textarea</label>
                <div class="col-sm-10">
                    <textarea rows="4" class="form-control" placeholder="Textarea"></textarea>
                </div>
            </div> --}}

        </form>
    </div>
@endsection
        