@extends('layouts.app')
@section('content')
<form  method="post" action="{{route('useremail')}}">
    <div class="box-typical box-typical-padding" > 
        {{csrf_field()}}
        <h2 style="font-weight:510; margin-bottom:20px;  font-size:28px; margin-left:0px;">Send mail To All Users</h2>
            <div class="form-group">
                <div class="col-lg-12">
                    <fieldset class="form-group">
                        <label class="form-label semibold" for="title">Subject</label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                        <small class="text-muted">Please give a subject to your Mail</small>
                    </fieldset>
                </div>
            </div>
            <div class="form-group" >
                <div class="col-lg-12" style="margin-bottom:40px;">
                    <fieldset class="form-group">
                        <label class="form-label semibold" for="message">Description</label>
                        <textarea rows="4" name="message" id="message" class="form-control" placeholder="Message"></textarea>
                    </fieldset>
                </div>
            </div>
        </div>
        
        <div class="box-typical box-typical-padding">
            <div class="box-typical-body">
                <div class="table-responsive project-tab" id="tabs">
                    <div class="container"> 
                        <div class="row">
                            <div class="col-md-12">
                            <header class="section-header">
                                <div class="tbl">
                                    <div class="tbl-row">
                                        <div class="tbl-cell">
                                            <h2 style="font-weight:510; margin-bottom:0px; margin-left:40px; margin-top:70px; font-size:28px; margin-left:0px;">Send Email</h2>
                                            {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </header>   
                            <nav>
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">                     
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><span style="color:#CF1719;">Users</a></span>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><span style="color:#CF1719;">Event Users</a></span>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><span style="color:#CF1719;">Business Users</a></span>
                                </div>
                            </nav>
                            <section class="card">
                                <div class="card-block">
                                    <div class="tab-content" id="nav-tabContent">
                                            {{-- <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th><input type="checkbox" id="selectall"/></th>
                                                            <th>Name</th>
                                                            <th>Email Id</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($user as $value)
                                                        <tr>
                                                            <td>{{$loop->index+1}}  
                                                            <td><input type="checkbox" class="checkBoxes" id="Checkbox1" name="checkbox[]"  value={{$value->user_id}}>
                                                            <td>{{$value->fname}}
                                                            <td>{{$value->email}}
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div> --}}
                                        {{-- user table --}}
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th><input type="checkbox" id="selectall"/></th>
                                                        <th>Name</th>
                                                        <th>Email Id</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($users as $value)
                                                    <tr>
                                                        <td>{{$loop->index+1}}  
                                                        <td><input type="checkbox" class="checkBoxes" id="Checkbox1" name="checkbox[]"  value={{$value->user_id}}>
                                                        <td>{{$value->fname}}
                                                        <td>{{$value->email}}
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        {{-- event table --}}
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th><input type="checkbox" id="selectall1"/></th>
                                                        <th>Name</th>
                                                        <th>Email Id</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($event_users as $value)
                                                    <tr>
                                                        <td>{{$loop->index+1}}      
                                                        <td><input type="checkbox" class="checkBoxes Checkbox2" id="Checkbox1" name="checkbox[]"  value={{$value->user_id}}>
                                                        <td>{{$value->fname}}
                                                        <td>{{$value->email}}
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        {{-- business table --}}
                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                            <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <th>#</th>
                                                    <th><input type="checkbox" id="selectall2"/></th>
                                                    <th>Name</th>
                                                    <th>Email Id</th>
                                                </thead>
                                                <tbody>
                                                    @foreach($business_users as $value)
                                                    <tr>
                                                        <td>{{$loop->index+1}}  
                                                        <td><input type="checkbox" class="checkBoxes Checkbox3" id="Checkbox1" name="checkbox[]"  value={{$value->user_id}}>
                                                        <td>{{$value->fname}}
                                                        <td>{{$value->email}}
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12" style="margin-bottom:0px; margin-top:15px;">
                                            <fieldset class="form-group">
                                                <input type="submit" value="Send Mail" class="btn-sm btn btn-danger">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
    		$('#selectall').on('click',function(){
        		if(this.checked)
                {
            		$('.checkBoxes').each(function(){
                		this.checked = true;
            	    });
        	    }
                else
                {
                    $('.checkBoxes').each(function(){
                        this.checked = false;
                    });
                }
            });

			$('.checkBoxes').on('click',function(){	
        		if($('.checkBoxes:checked').length == $('.checkBoxes').length){
            		$('#selectall').prop('checked',true);
        		}else{
            		$('#selectall').prop('checked',false);
        		}
    		});
    	});
    </script>
    <script>
        $(document).ready(function(){
    		$('#selectall1').on('click',function(){
        		if(this.checked)
                {
            		$('.Checkbox2').each(function(){
                		this.checked = true;
            	    });
        	    }
                else
                {
                    $('.Checkbox2').each(function(){
                        this.checked = false;
                    });
                }
            });

			$('.Checkbox2').on('click',function(){	
        		if($('.Checkbox2:checked').length == $('.Checkbox2').length){
            		$('#selectall1').prop('checked',true);
        		}else{
            		$('#selectall1').prop('checked',false);
        		}
    		});
    	});
    </script>
    <script>
        $(document).ready(function(){
            $('#selectall2').on('click',function(){
                if(this.checked)
                {
                    $('.Checkbox3').each(function(){
                        this.checked = true;
                    });
                }
                else
                {
                    $('.Checkbox3').each(function(){
                        this.checked = false;
                    });
                }
            });

            $('.Checkbox3').on('click',function(){	
                if($('.Checkbox3:checked').length == $('.Checkbox3').length){
                    $('#selectall2').prop('checked',true);
                }else{
                    $('#selectall2').prop('checked',false);
                }
            });
        });
    </script>
    <script type="text/javascript">
        function check()
        {
            var temp = [];

            var users = document.getElementsByClassName('checkBoxes');
            //alert(users);
            //console.log(users);
            var message = document.getElementById('message');
            //alert(message);

            var subject = document.getElementById('subject').value;

            for(let i=0;  i<users.length;  i++)
            {   
                var demo = document.getElementsByClassName("checkBoxes")[i].checked;

                if(demo)
                {
                    temp.push(document.getElementsByClassName("checkBoxes")[i].value);
                }            
            }           
        }
    </script>
@endsection


{{-- <section class="card">
                        <div class="card-block">
                            <form>
                                <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th><input type="checkbox" id="selectall"/></th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    {{-- <th>Action</th> 
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($user as $value)
                                    <tr>
                                        <td>{{$loop->index+1}}  
                                        <td><input type="checkbox" class="checkBoxes" id="Checkbox1" name="checkbox[]"  value={{$value->user_id}}>
                                        <td>{{$value->fname}}
                                        <td>{{$value->email}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- <a class="btn-sm btn btn-danger"  href=""><span style= "font-size:20px;" >Send Email</a> 

                            <div class="form-group">
                                <div class="col-lg-12" style="margin-bottom:40px;">
                                    <fieldset class="form-group">
                                        <input type="submit" value="Send Mail" class="btn-sm btn btn-danger">
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </section> --}}


                    {{-- <header class="section-header">
                        <div class="tbl">
                            <div class="tbl-row">
                                <div class="tbl-cell">
                                    <h2 style="font-weight:510; margin-bottom:20px;  font-size:28px; margin-left:0px;">Send Email</h2>
                                    {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> 
                                </div>
                            </div>
                            <div class="tbl-row" style="color:#F6F8FA;">
                                <div class="tbl-cell">
                                    <a class="btn-sm btn btn-danger"   href= "{{route('usermail')}}"><span style= "font-size:20px;" >Users</a>
                                    <a class="btn-sm btn btn-danger"   href= "{{route('eventmail')}}"><span style= "font-size:20px;" >Event User</a>    
                                    <a class="btn-sm btn btn-danger"  href="{{route('businessmail')}}"><span style= "font-size:20px;" >Business User</a>
                                </div>
                            </div>
                        </div>
                    </header> --}}