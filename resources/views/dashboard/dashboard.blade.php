@extends('layouts.app')
@section('content')
  
        <div class="row">
                {{-- 1st --}}
                <div class="col-xxl-4 col-sm-4">
                <section class="widget widget-tabs-compact">
                        <div class="tab-content widget-tabs-content" style="min-height:20px;">
                                <div class="tab-pane active" id="w-4-tab-1" role="tabpanel">
                                        <div class="user-card-row">
                                                <div class="tbl-row">
                                                        <div class="tbl-cell tbl-cell-photo tbl-cell-photo-64">
                                                                <a href="#">
                                                                <i style="color:#CF0336; font-size: 1.50em;" class="glyphicon glyphicon-list-alt"></i>
                                                                </a>
                                                        </div>
                                                        <div class="tbl-cell">
                                                                <p class="user-card-row-name font-16"><a href="{{route('userreg-show')}}" >Users</a></p>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="widget-tabs-nav colored">
                                <ul class="tbl-row">
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Today</p>
                                                        <p style="font-size:22px;">
                                                        {{$today}}
                                                        </p>
                                                </a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Week</p>
                                                        <p style="font-size:22px;">{{$week}}</p>
                                                </a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Month</p>
                                                        <p style="font-size:22px;">{{$month}}</p>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                </section><!--.widget-tabs-compact-->
                </div>
                {{-- 2nd --}}
                <div class="col-xxl-4 col-sm-4">
                <section class="widget widget-tabs-compact">
                        <div class="tab-content widget-tabs-content" style="min-height:20px;">
                                <div class="tab-pane active" id="w-4-tab-1" role="tabpanel">
                                        <div class="user-card-row">
                                                <div class="tbl-row">
                                                        <div class="tbl-cell tbl-cell-photo tbl-cell-photo-64">
                                                                <a href="">
                                                                <i style="color:#CF0336; font-size: 1.50em;" class="glyphicon glyphicon-duplicate"></i>
                                                                </a>    
                                                        </div>
                                                        <div class="tbl-cell">
                                                                <p class="user-card-row-name font-16"><a href="{{route('business')}}">Business</a></p>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="widget-tabs-nav colored">
                                <ul class="tbl-row">
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Today</p>
                                                <p style="font-size:22px;">{{$business_today}}</p>
                                                </a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Week</p>
                                                <p style="font-size:22px;">{{$business_week}}</p>
                                                </a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Month</p>
                                                        <p style="font-size:22px;">{{$business_month}}</p>
                                                </a>
                                        </li>
                                </ul>
                        </div> 
                </section>
                </div> 
                {{-- 3nd --}}
                <div class="col-xxl-4 col-sm-4">
                <section class="widget widget-tabs-compact">
                        <div class="tab-content widget-tabs-content" style="min-height:20px;">
                                <div class="tab-pane active" id="w-4-tab-1" role="tabpanel">
                                        <div class="user-card-row">
                                                <div class="tbl-row">
                                                        <div class="tbl-cell tbl-cell-photo tbl-cell-photo-64">
                                                                <a href="#">
                                                                <i style="color:#CF0336; font-size:1.50em;" class="font-icon font-icon-widget"></i>
                                                                </a>
                                                        </div>
                                                        <div class="tbl-cell">
                                                                <p class="user-card-row-name font-16"><a href="{{route('event')}}">Events</a></p>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="widget-tabs-nav colored">
                                <ul class="tbl-row">
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Today</p>
                                                        <p style="font-size:22px;">{{$event_today}}</p>
                                                </a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Week</p>
                                                        <p style="font-size:22px;">{{$event_week}}</p>
                                                </a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link red" style="height:60px !important; background-color: #CF0336 !important;">
                                                        <p style="margin-bottom:0px !important;">Month</p>
                                                        <p style="font-size:22px;">{{$event_month}}</p>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                </section><!--.widget-tabs-compact-->
                </div>       
        </div> 
    
        <section class="card box-typical">
                <div class="card-block">
                        <h5 class="m-t-lg pull-left"><strong>Users</strong></h5>
                        {{-- <form method="POST" id="dr_Form">
                                <div class="col-3 pull-right">
                                <div class="form-group">
                                        <strong>To:</strong><input class="flatpickr form-control" id="dr_date2" name="dr_date2" type="text" placeholder="Select Date">
                                </div>
                                </div>
                                <div class="col-3 pull-right">
                                <div class="form-group">
                                        <strong>From:</strong><input class="flatpickr form-control" id="dr_date1" name="dr_date1" type="text" placeholder="Select Date">
                                </div>
                                </div>
                        </form> --}}
                        <div id="app_wrapper">
                                <canvas id="user_chart" width="500" height="100"></canvas>
                        </div>
                                
                </div>
        </section>

        <section class="card box-typical">
                <div class="card-block">
                        <h5 class="m-t-lg pull-left"><strong>Events</strong></h5>
                        {{-- <form method="POST" id="dr_Form">
                                <div class="col-3 pull-right">
                                <div class="form-group">
                                        <strong>To:</strong><input class="flatpickr form-control" id="dr_date2" name="dr_date2" type="text" placeholder="Select Date">
                                </div>
                                </div>
                                <div class="col-3 pull-right">
                                <div class="form-group">
                                        <strong>From:</strong><input class="flatpickr form-control" id="dr_date1" name="dr_date1" type="text" placeholder="Select Date">
                                </div>
                                </div>
                        </form> --}}
                        <div id="app_wrapper">
                                <canvas id="event_chart" width="500" height="100"></canvas>
                        </div>
                                
                </div>
        </section>

        <section class="card box-typical">
                <div class="card-block">
                        <h5 class="m-t-lg pull-left"><strong>Business</strong></h5>
                        {{-- <form method="POST" id="dr_Form">
                                <div class="col-3 pull-right">
                                <div class="form-group">
                                        <strong>To:</strong><input class="flatpickr form-control" id="dr_date2" name="dr_date2" type="text" placeholder="Select Date">
                                </div>
                                </div>
                                <div class="col-3 pull-right">
                                <div class="form-group">
                                        <strong>From:</strong><input class="flatpickr form-control" id="dr_date1" name="dr_date1" type="text" placeholder="Select Date">
                                </div>
                                </div>
                        </form> --}}
                        <div id="app_wrapper">
                                <canvas id="business_chart" width="500" height="100"></canvas>
                        </div>
                                
                </div>
        </section>
@endsection
@section('scripts')
       
        <script>
              $(document).ready(function(){
                     Chart.defaults.global.defaultFontColor = 'grey';
                     Chart.defaults.global.defaultFontStyle = 'bold';
                     Chart.defaults.global.defaultFontSize = 13;
                     events();
                     business();   
                     user();
              });
       
              function events()
              {
                     var ajx = new XMLHttpRequest();
                     ajx.onreadystatechange = function() {
                            if(ajx.readyState == 4 && ajx.status == 200){
                                   var res = JSON.parse(ajx.responseText);                                        
                                   var data = {
                                          labels: res.dateLabel,
                                          datasets:[{
                                                 label:'Events',
                                                 fill: true, 
                                                 tension: 0.4,                           
                                                 backgroundColor: "#CF0336",
                                                 borderColor: "#898a7c",
                                                 borderCapStyle: 'butt',
                                                 borderDash: [],
                                                 borderDashOffset: 0.0,
                                                 borderJoinStyle: 'miter',
                                                 pointBorderColor: "white",
                                                 pointBackgroundColor: "black",
                                                 pointBorderWidth: 1,
                                                 pointHoverRadius: 5,
                                                 pointHoverBackgroundColor: "yellow",
                                                 pointHoverBorderColor: "green",
                                                 pointHoverBorderWidth: 2,
                                                 pointRadius: 4,
                                                 pointHitRadius: 10,
                                                 data:res.events,
                                                 spanGaps: true,
                                          }]
                                   }
                                   //Start Chart plotting.
                                   var ctx = $('#event_chart');
                                   var myLineChart = new Chart(ctx, {
                                          type : 'line',
                                          data : data
                                   })
                            }
                     }
                     
                     ajx.open('GET','{{route("event_chart")}}',true);
                     ajx.send();
              }

              function business()
              {
                     var ajx = new XMLHttpRequest();
                     ajx.onreadystatechange = function() {
                            if(ajx.readyState == 4 && ajx.status == 200){
                                   var res = JSON.parse(ajx.responseText);                                        
                                   var data = {
                                          labels: res.dateLabel,
                                          datasets:[{
                                                 label:'Business',
                                                 fill: true, 
                                                 tension: 0.4,                           
                                                 backgroundColor: "#CF0336",
                                                 borderColor: "#898a7c",
                                                 borderCapStyle: 'butt',
                                                 borderDash: [],
                                                 borderDashOffset: 0.0,
                                                 borderJoinStyle: 'miter',
                                                 pointBorderColor: "white",
                                                 pointBackgroundColor: "black",
                                                 pointBorderWidth: 1,
                                                 pointHoverRadius: 5,
                                                 pointHoverBackgroundColor: "yellow",
                                                 pointHoverBorderColor: "green",
                                                 pointHoverBorderWidth: 2,
                                                 pointRadius: 4,
                                                 pointHitRadius: 10,
                                                 data:res.business,
                                                 spanGaps: true,
                                          }]
                                   }
                                   //Start Chart plotting.
                                   var ctx = $('#business_chart');
                                   var myLineChart = new Chart(ctx, {
                                          type : 'line',
                                          data : data
                                   })
                            }
                     }
                     
                     ajx.open('GET','{{route("business_chart")}}',true);
                     ajx.send();
              }

              function user()
              {
                     var ajx = new XMLHttpRequest();
                     ajx.onreadystatechange = function() {
                            if(ajx.readyState == 4 && ajx.status == 200){
                                   var res = JSON.parse(ajx.responseText);                                        
                                   var data = {
                                          labels: res.dateLabel,
                                          datasets:[{
                                                 label:'Users',
                                                 fill: true, 
                                                 tension: 0.4,                           
                                                 backgroundColor: "#CF0336",
                                                 borderColor: "#898a7c",
                                                 borderCapStyle: 'butt',
                                                 borderDash: [],
                                                 borderDashOffset: 0.0,
                                                 borderJoinStyle: 'miter',
                                                 pointBorderColor: "white",
                                                 pointBackgroundColor: "black",
                                                 pointBorderWidth: 1,
                                                 pointHoverRadius: 5,
                                                 pointHoverBackgroundColor: "yellow",
                                                 pointHoverBorderColor: "green",
                                                 pointHoverBorderWidth: 2,
                                                 pointRadius: 4,
                                                 pointHitRadius: 10,
                                                 data:res.user,
                                                 spanGaps: true,
                                          }]
                                   }
                                   //Start Chart plotting.
                                   var ctx = $('#user_chart');
                                   var myLineChart = new Chart(ctx, {
                                          type : 'line',
                                          data : data
                                   })
                            }
                     }
                     
                     ajx.open('GET','{{route("user_chart")}}',true);
                     ajx.send();
              }
        
       </script>
@endsection


      
