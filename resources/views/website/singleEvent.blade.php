@extends('website.inc.app')

@section('content')
    

    <!-- content and image section -->
    <section id="single_home" style="height: 400px !important;" >
        <div class="card mb-3">
            <img class="card-img-top " src="{{asset('website_assets/image/single_event.jpg')}}" alt="Card image cap">
        </div>
    </section>
    <!-- content and image sectioon ends -->

    {{-- alert
    <div class="alert display alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
        
    <div class="alert display alert-danger alert-dismissible fade show mb-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div> --}}

    <!-- single event -->
    <section id="single_event">
        <div class="container single-event">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-8">
                    <div id="single_event_info" class="row">
                        <div class="col-md-9 text-left">
                            <h4 id="event_heading1"></h4>
                            <!-- <p><i style="color:crimson;" class="fa fa-map-marker" aria-hidden="true"></i> Ahmedabad</p> -->
                        </div>
                        <div class="col-md-3 event-button text-right">
                            @if(session('user_type') == 2)
                                <div class="apply-event">
                                    
                                    <a href="{{route('event-select',['id' =>$id1])}}" id="apply" class="btn btn-login">Apply</a>
                                </div>
                            @elseif(session('user_type') == 2)
                                
                            @else
                                
                            @endif
                        </div>
                        <hr>
                        <div class="col-md-6 text-left">
                            <div class="single_event_time">27 Dec-29 Dec &nbsp; &nbsp;<i style="color:crimson;" class="fa fa-map-marker" aria-hidden="true"></i> <span id="address">Ahmedabad</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
    <!-- single event ends -->

    <!-- event description -->
    <section id="single-event-description">
        <div class="container description">
            <div class="row">
                <div class="col-md-9">
                    <div class="Description ">
                        <h4>Event <span>Description</span></h4>
                        <p class="event_description">Sunburn 2019 is back in Goa once again to make you relive the time of your life. The Sunburn festival is famous for its amazing music and commendable artist line-up, who make sure you have a great time at the music festival. Bask in the glory of Goa while being soaked in the mesmerizing music of the concert. No matter if it's your first time to the festival or the 10th, Sunburn promises a memorable experience for you and your friends! Not to mention, Instagram feeds for days. Hurry, book your tickets to the Sunburn festival now!</p>

                        <span id="start_date" style="margin-bottom:10px;"> </span> &nbsp;<span id="end_date"></span><br>
                        <span id="start_time"> </span> &nbsp;<span id="end_time"></span>
                    </div>
                    <hr>
                    <h5 class="text-left" style="margin-bottom:20px; color:#CF1719;">Gallery</h5>
                    <div class="gallery owl-carousel owl-theme">
                        {{-- <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl zoom img-fluid "   src="/image/gallery.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"> <img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div> --}}
                    </div>
                    <!-- <div class="social-share">
                        <h4>Share This Event</h4>
                        <a><i class="fa fa-facebook" aria-hidden="true"></i>
                        </a> &nbsp;  &nbsp;  <a><i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </div> -->
                </div>
                <div class="col-md-3">
                    <div class="social-share">
                        <h4>Share This Business</h4>
                        <a><i class="fa fa-facebook icon" aria-hidden="true"></i>
                        </a> &nbsp;  &nbsp;  <a><i  class="fa fa-twitter icon" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- <div class="postar">
                        <img src="image/poster1.webp" alt="image" class="img-fluid">
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- event description ends -->

    <!-- business gallery -->
    <section id="single-event-description" class="Businesss">
        <div class="container description">
            <div class="row">
                <div class="col-md-9 business">  
                    <h2 class="text-left" style="margin-bottom:20px; color:#CF1719;">Business</h2>
                    <div class="gallery gallery2 owl-carousel owl-theme">
                        {{-- <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="/image/gallery.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"> <img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div> --}}
                    </div>
                    <!-- <div class="social-share">
                        <h4>Share This Event</h4>
                        <a><i class="fa fa-facebook" aria-hidden="true"></i>
                        </a> &nbsp;  &nbsp;  <a><i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- business gallery ends -->
@endsection
@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <script src="{{asset('website_assets/singleapp.js')}}"></script>

    <script>
        /*....................magnifire....................*/

        // $(function () {
        //     $('.card').magnificPopup({
        //         delegate: 'a', // child items selector, by clicking on it popup will open
        //         type: 'image'
        //         // other options
        //     });
        // });
    
        let object = {
            event_id : "{{$id1}}",
            _token : "{{csrf_token()}}"
        }

        console.log(object);

        $(function(){
            $.ajax({
                url : "http://34.202.173.112/smartevent/Event/detail",
                type: "POST",
                data: object,
                dataType: "json",
                success : function(data){
                    
                    let text = data;
                    console.log(text);

                    if(text.status == 1)
                    {
                        let event = data.data;
                        $('#event_heading1').html(event.event_name);
                        $('.single_event_time').html(event.start_date)
                        $('#address').html(event.address);
                        $('.event_description').html(event.description);
                        $('#start_time').html(event.start_time);
                        $('#end_time').html(event.end_time);
                        $('#start_date').html(event.start_date);
                        $('#end_date').html(event.end_date);
                        $('.gallery').html('');
                        $.each(event.images,function(index,item){
                            $('.gallery').append(`
                                <div class="card" style="width: 18rem; margin-left:0px">
                                    <a href="${item}"><img style="height: 250px; width:270px;" class="card-img-top owl zoom img-fluid " src="${item}" alt="Card image cap"></a>
                                </div>
                            `);
                        });

                        if(text.business = []){
                            $('.Businesss').addClass('display');
                        }
                        else
                        {   
                            $('.Businesss').removeClass('display');
                            let businesss = text.business.business;

                            $.each(businesss.images,function(item,index){
                                $('. gallery2').append(`
                                    <div class="card" style="width: 18rem; margin-left:0px">
                                        <a href="${item}"><img style="height: 250px; width:270px;" class="card-img-top owl zoom img-fluid " src="${item}" alt="Card image cap"></a>
                                    </div>
                                `);
                            });
                        }
                    }   
                    else
                    {
                        $('.alert-danger').removeClass('display');
                        $('.alert-danger').addClass('visible');
                        $('.message').html(text.msg);
                    }
                }
            }); 
        });

    </script>
@endsection
