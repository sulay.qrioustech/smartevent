@extends('website.inc.app')
@section('content')
     <!-- preloader -->
    <section id="preloader">
        <div id="pre">  
            &nbsp;
        </div>
    </section>
    <!-- preloader end -->

    <!-- content and image section -->
    <section id="home" class="owl-carousel owl-theme">
        <div class="card mb-3">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image5.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image6.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image3.jpg')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image1')}}" alt="Card image cap">
          </div>
    </section>

    <!-- upcomming and past event -->
    <section id="event">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-12">
                        <div id="event_left">
                            <div class="poster_image">
                                <img src="image/advertisement3.jpg" class="img-fluid imagespostar" alt="image">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-12">
                        <div id="event_right row">
                            <!-- upcomming -->
                            <div class="event_heading col-md-12">
                                <h4 class="text-left">My <span style="font-size:28px;"> <b>Events</b></span></h4>
                            </div>
                            <div id="upcommingbusiness" class="upcommingevents owl-carousel owl-theme">
                                {{-- <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                      <a href="singleEvent.html"><img src="image/kabirsingh.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Nakshatra</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/gameover.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>food</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/kabirsingh.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Iskara</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/bharat.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Anatarya</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleEvent.html"><img src="image/gameover.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading"><p>Aroha</p></div>
                                        <div class="date">Dec 5 2019</div>
                                    </div> 
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcomming and past event ends -->
@endsection

@section('scripts')
    <script>
        $(function(){
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);
            let object = {
                business_id : id,
                _token : "{{csrf_token()}}"
            }   
            console.log(object)
            $.ajax({
                url : "http://34.202.173.112/smartevent/select/events/list",
                method : "post",
                data : object,
                dataType : "json",
                success : function(data){
                    let response = data.event_list;
                    console.log(response);
                    $('#upcommingbusiness').html('');
                    $.each(response,function(index,item){
                        $('#upcommingbusiness').append(`
                            <div class="col-md-4 demo owl">
                                <div class="event_image">
                                    <a href="http://34.202.173.112/smartevent/single/event/${item.events.event_id}"><img src="${item.events.logo_image}" alt="images"/></a>
                                </div>
                                <div class="image_description">
                                    <div class="favourites" style="font-size:16px;">${item.events.event_name}</div>
                                        
                                    <div class="date">${item.events.start_date}</div>
                                </div> 
                            </div>
                        `);
                    });
                }
            });
        });
        
    </script>
@endsection