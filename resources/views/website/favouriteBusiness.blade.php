@extends('website.inc.app')
@section('content')
     <!-- preloader -->
    <section id="preloader">
        <div id="pre">  
            &nbsp;
        </div>
    </section>
    <!-- preloader end -->

    <!-- content and image section -->
    <section id="home" class="owl-carousel owl-theme">
        <div class="card mb-3">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image5.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image6.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image3.jpg')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image1')}}" alt="Card image cap">
          </div>
    </section>

    <!-- upcomming and past event -->
    <section id="event">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-12">
                        <div id="event_left">
                            <div class="poster_image">
                                <img src="image/advertisement3.jpg" class="img-fluid imagespostar" alt="image">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div id="event_right row">
                            <!-- upcomming -->
                            <div class="event_heading col-md-12">
                                <h4 class="text-left"  style=" margin-top:1
                                0px; color:#cf1718 !important;">Business</h4>
                            </div>
                            <div id="upcommingbusiness" class="upcommingevents owl-carousel owl-theme">
                                {{-- <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business1.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Qatello</p></div>
                                        <div>&nbsp;</div>
                                    
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business2.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Voroly</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business3.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Monicy</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business4.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Rumpes</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/gameover.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Nultat</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcomming and past event ends -->
@endsection

@section('scripts')
    <script>
        let user_id ="{{session('user_id')}}"

        $(function(){

            let object = {
                user_id : "{{session('user_id')}}",
                _token : "{{csrf_token()}}"
            }
            console.log(object)

            $.ajax({
                url: "http://34.202.173.112/smartevent/Business/list",
                type: "POST",
                data: object,
                dataType: "json",
                success : function(data){
                    let response = data.data;
                    console.log(response);
                    $('#upcommingbusiness').html('');
                    $.each(response,function(index,item){
                        $('#upcommingbusiness').append(`
                            <div class="col-md-4 demo owl">
                                <div class="event_image">
                                    <a href="http://34.202.173.112/smartevent/business/${item.business_id}"><img src="${item.logo_image}" alt="images"/></a>
                                </div>
                                <div class="image_description">
                                    <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;"></span></i></div>
                                    <div class="heading" style="margin-top:10px;"><p>Nultat</p></div>
                                    <div>&nbsp;</div>
                                </div> 
                            </div>
                        `);
                    });
                }
            });
        });
        

    </script>
@endsection