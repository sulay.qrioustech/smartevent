@extends('website.inc.app')

@section('content') 

    <!-- content and image section -->
    <section id="single_home" style="height: 400px !important;" >
        <div class="card mb-3">
            <img class="card-img-top " src="{{asset('website_assets/image/single_event.jpg')}}" alt="Card image cap">
        </div>
    </section>
    <!-- content and image sectioon ends -->

    {{-- alert --}}
    <div class="alert  display alert-success alert-dismissible fade show" style="margin-top:50px;" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
        
    <div class="alert display alert-danger alert-dismissible fade show mb-0" style="margin-top:50px;"  role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>

    <!-- single event -->
    <section id="single_event">
        <div class="container single-event">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-8">
                    <div id="single_event_info" class="row">
                        <div class="col-md-9 text-left">
                            <h4 id="event_heading1"></h4>
                            <!-- <p><i style="color:crimson;" class="fa fa-map-marker" aria-hidden="true"></i> Ahmedabad</p> -->
                        </div>
                        <div class="col-md-3 event-button text-right">
                                @if(session('user_type') == 1)
                            
                                @elseif(session('user_type') == 2)
                                    
                                @else
                                    <div class="apply-event like"> 
                                        <a href="{{route('event-select',['id' =>$id1])}}" id="Like" class="btn btn-sm btn-login">Like</a>
                                    </div>
                                    {{-- <div class="apply-event dislike display"> 
                                        <a href="{{route('event-select',['id' =>$id1])}}" id="disLike" class="btn btn-sm btn-login">disLike</a>
                                    </div> --}}
                                @endif
                        </div>
                        <hr>
                        <div class="col-md-6 text-left">
                            <div class="single_event_time"><i class="fa fa-map-marker" aria-hidden="true"></i> Ahmedabad</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
    <!-- single event ends -->

    <!-- event description -->
    <section id="single-event-description">
        <div class="container description">
            <div class="row">
                <div class="col-md-9">
                    <div class="Description ">
                        <h4>Business <span>Description</span></h4>
                        <p class="event_description"></p> 

                        <span id="start_date" style="margin-bottom:10px;"> </span> &nbsp;<span id="end_date"></span><br>
                        <span id="start_time"> </span> &nbsp;<span id="end_time"></span>
                    </div>
                    <hr>
                    <h5 class="text-left" style="margin-bottom:20px; color:#CF1719;">Gallery</h5>
                    <div class="gallery owl-carousel owl-theme">
                        {{-- <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl zoom img-fluid "   src="/image/gallery.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"> <img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div>
                        <div class="card" style="width: 18rem; margin-left:0px">
                            <a href="/image/gallery.jpeg"><img style="height: 250px; width:270px;" class="card-img-top owl" src="image/gallery3.jpeg" alt="Card image cap"></a>
                        </div> --}}
                    </div>
                    <!-- <div class="social-share">
                        <h4>Share This Event</h4>
                        <a><i class="fa fa-facebook" aria-hidden="true"></i>
                        </a> &nbsp;  &nbsp;  <a><i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </div> -->
                </div>
                <div class="col-md-3">
                    <div class="social-share">
                        <h4>Share This Business</h4>
                        <a><i class="fa fa-facebook icon" aria-hidden="true"></i>
                        </a> &nbsp;  &nbsp;  <a><i  class="fa fa-twitter icon" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- <div class="postar">
                        <img src="image/poster1.webp" alt="image" class="img-fluid">
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- event description ends -->
@endsection
@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <script src="{{asset('website_assets/singleapp.js')}}"></script>

    <script>
        /*....................magnifire....................*/

        $(function () {
            $('.card').magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image'
                // other options
            });
        });
       
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        let object = {
            business_id : id,
            _token : "{{csrf_token()}}"
        }
        //console.log(object);

        $(function(){
            $.ajax({
                url : "http://34.202.173.112/smartevent/Business/details",
                type: "POST",
                data: object,
                dataType: "json",
                success : function(data){
                    
                    let text = data;
                    console.log(text);

                    if(text.status == 1)
                    {
                        let user = data.data;
                        let business = data.business;
                        $('#event_heading1').html(business.company_name);
                        $('.single_event_time').html(user.address);
                        $('.event_description').html(business.description);
                        $('.gallery').html('');
                        // console.log(business.images[0]);
                        $.each(business.images,function(item,index){
                            $('.gallery').append(`
                                <div class="card" style="width: 18rem; margin-left:0px">
                                    <a href="${item}"><img style="height: 250px; width:270px;" class="card-img-top owl zoom img-fluid " src="${item}" alt="Card image cap"></a>
                                </div>
                            `);
                        });
                    }   
                    else
                    {
                        $('.alert-danger').removeClass('display');
                        $('.alert-danger').addClass('visible');
                        $('.message').html(text.msg);
                    }
                }
            }); 
        });

        var url1 = window.location.pathname;
        var u_id = url.substring(url1.lastIndexOf('/') + 1);
        
        let favourite = {
            user_id : "{{session('user_id')}}",
            business_id : u_id,
            _token : "{{csrf_token()}}"
        }

        $(function(){
            $(document).on('click','#Like',function(e){
                e.preventDefault();
                $.ajax({
                    url : "http://34.202.173.112/smartevent/favourite",
                    type : "post",
                    data : favourite,
                    dataType : "json",
                    success : function(data)
                    {
                        let response = data;
                        console.log(response);

                        if(response.status == 1)
                        {
                            $('.alert-success').removeClass('display');
                            $('.alert-danger').removeClass('visible');
                            $('.message').html(response.msg);
                            $('#Like').html('dislike');

                            let favourite_id1 = {
                                favourite_id : response.favourite_id,
                                _token : "{{csrf_token()}}"
                            }
                            $(document).on('click','#Like',function(e){
                                e.preventDefault();
                                $.ajax({
                                    url : "http://34.202.173.112/smartevent/favourite/delete",
                                    type : "post",
                                    data : favourite_id1,
                                    dataType : "json",
                                    success : function(data)
                                    {
                                        let response = data;
                                        console.log(response);

                                        if(response.status == 1)
                                        {
                                            $('.alert-success').removeClass('display');
                                            $('.alert-danger').removeClass('visible');
                                            $('.message').html(response.msg);
                                            $('#Like').html('Like');
                                        }
                                    }
                                });
                            });
                        }
                        // else
                        // {
                        //     $('.alert-danger').removeClass('display');
                        //     $('.alert-danger').removeClass('visible');
                        //     $('.message').html(response.msg);
                        //     $('.like').removeClass('display');
                        //     $('.dislike').addClass('display');
                        // }
                    }
                });
            });
        });
    </script>
@endsection
