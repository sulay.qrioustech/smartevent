<!-- Footer -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left text" style="font-size:18px;">© 2019 <span style="color:#CD2129;">Sultanimakutano</span> PTE LTD</div>
            <div class="col-md-6 text-right" id="socials">
                <a href="" class="link" target="_blank"><i style="color:#3B579D" class="fa fa-facebook-square" aria-hidden="true"></i></a>
                <a href="" class="link" target="_blank"><i style="color:#40B6F1" class="fa fa-twitter-square" aria-hidden="true"></i></a>
                <a href="" class="link" target="_blank"><i style="color:#D42375" class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="" class="link" target="_blank"><i style="color:#2C74B3" class="fa fa-linkedin" aria-hidden="true"></i></a>
                <a href="" class="link" target="_blank"><i style="color:#CD2129" class="fa fa-pinterest-square" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Ends-->