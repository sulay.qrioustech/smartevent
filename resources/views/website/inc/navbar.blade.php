
<!-- header -->
@if(session('user_type') == 1)
    {{-- header --}}
    <header>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="navbar-nav">
                    <a class="navbar-brand smooth-scroll" href="#home">
                        <img src="{{asset('website_assets/image/mainlogo.png')}}" class="img-fluid" alt="logo" id="img-gone">
                    </a>
                </div>
                <div id="navbar-02">
                    <button id="logo-mobile" class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <button type="button" id="home-close" class="close btn-close " aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                        <!-- <span aria-hidden="true">&times;</span> -->
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item ">
                                <span class="nav-link active" id="navbarDropdownMenuLink">
                                    <a href="{{route('website-home')}}">Home
                                    </a>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link " id="navbarDropdownMenuLink">
                                    <a href="{{route('website-home')}}#event">Events
                                    </a>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link " id="navbarDropdownMenuLink">
                                    <a href="{{route('Business-List')}}">Business
                                    </a>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link" id="navbarDropdownMenuLink">
                                        <a href="" data-target="#Create_Event" data-toggle="modal">Create Event
                                        </a>
                                </span>
                                <div id="Create_Event" class="modal fade" role="dialog">
                                    <div class="modal-dialog"> 
                                        <div class="modal-content">
                                        <div class="modal-body">
                                            <button data-dismiss="modal" class="close">&times;</button>
                                            <h4>Create Event</h4>
                                            <form method="post" id="create_event" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{session('user_id')}}" name="user_id" class="user_id"/>
                                                <small class="float-left">Event Name</small>
                                                <input type="text" name="event_name" class=" form-control"/>
                                                <small class="float-left">Description</small>
                                                <input type="text" name="description" class=" form-control"/>
                                                <small class="float-left">Select Logo</small>
                                                <input type="file" name="logo_image" class="form-control">
                                                <small class="float-left">Event Organizar Name</small>
                                                <input type="text" name="event_organizar_name" class=" form-control"/>
                                                <small class="float-left">Start Date</small>
                                                <input type="text" name="start_date" class=" form-control" />
                                                <small class="float-left">End Date</small>
                                                <input type="text" name="end_date" class=" form-control" />
                                                <small class="float-left">Start Time</small>
                                                <input type="text" name="start_time" class=" form-control">
                                                <small class="float-left">End Time</small>
                                                <input type="text" name="end_time" class=" form-control" />
                                                <small class="float-left">Select Multiple Image</small>
                                                <input type="file" multiple name="images[]" class="form-control"/>
                                                <small class="float-left">Address</small>
                                                <textarea class=" form-control" name="address" ></textarea>
                                                <small class="float-left">Phone Number</small>
                                                <input type="text" name="phone_number" class=" form-control"/>
                                                <input class="btn login" type="submit" value="Submit"/><br>
                                            </form> 
                                            {{-- alert --}}
                                            <div class="alert display alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
    
                                            <div class="alert display alert-danger alert-dismissible fade show mb-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>
                                        </div>
                                    </div>
                                </div>
                            </li> 
                            <li class="nav-item">
                                <span class="nav-link" id="navbarDropdownMenuLink">
                                    <a href="{{route('website-home')}}#inquiry">
                                        Inquiry
                                    </a>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link " id="navbarDropdownMenuLink">
                                    <a href="{{route('website-contact')}}">Contact
                                    </a>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link " id="navbarDropdownMenuLink">
                                    <a href="{{route('website-logout')}}" style="color:white !important; font-size:16px;" class="btn btn-sm btn-login">Logout
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!-- header ends -->
@elseif(session('user_type') == 2)
    {{-- header starts --}}
    <header>
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="navbar-nav">
                        <a class="navbar-brand smooth-scroll" href="#home">
                            <img src="{{asset('website_assets/image/mainlogo.png')}}" class="img-fluid" alt="logo" id="img-gone">
                        </a>
                    </div>
                    <div id="navbar-02">
                        <button id="logo-mobile" class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <button type="button" id="home-close" class="close btn-close " aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <!-- <span aria-hidden="true">&times;</span> -->
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item ">
                                    <span class="nav-link active" id="navbarDropdownMenuLink">
                                        <a href="{{route('website-home')}}">Home
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        @php
                                            $user_id = session('user_id');
                                            $business = App\BusinessCreate::Select('business_id')->where('user_id',$user_id)->first();
                                        @endphp
                                        <a href="{{route('myevents',['id' => $business])}}">My Events
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('Business-List')}}">Business
                                        </a>
                                    </span>
                                </li>
                                <li class=" nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="" data-target="#Create_Business" data-toggle="modal">Create Business
                                        </a>
                                    </span>
                                    <div id="Create_Business" class="modal fade" role="dialog">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content">
                                            <div class="modal-body">
                                                <button data-dismiss="modal" class="close">&times;</button>
                                                <h4>Create Business</h4>
                                                <form method="post" id="create_business">
                                                    {{csrf_field()}}
                                                    <input type="hidden" value="{{session('user_id')}}" name="user_id" class="user_id"/>
                                                    <small class="float-left">Company Name</small>
                                                    <input type="text" name="company_name" class=" form-control"/>
                                                    <small class="float-left">Select Logo</small>
                                                    <input type="file" name="logo_image" class="form-control">
                                                    <small class="float-left">Select Multiple Image</small>
                                                    <input type="file" multiple name="images[]" class="form-control">
                                                    <small class="float-left">Description</small>
                                                    <input type="text" name="description" class="form-control" />
                                                    <input class="btn login" type="submit" value="Submit"/><br>
                                                </form> 

                                                {{-- alert --}}
                                                <div class="alert display alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>

                                                <div class="alert display alert-danger alert-dismissible fade show mb-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link" id="navbarDropdownMenuLink">
                                        <a href="{{route('website-home')}}#inquiry">
                                            Inquiry
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('website-contact')}}">Contact
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('website-logout')}}" style="color:white !important; font-size:16px;" class="btn btn-sm btn-login">Logout
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
    </header>
    <!-- header ends -->

@elseif(session('user_type') == 3)
    {{-- header starts --}}
    <header>
            <div class="container-fluid" style="margin-left:180px;">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="navbar-nav">
                        <a class="navbar-brand smooth-scroll" href="#home">
                            <img src="{{asset('website_assets/image/mainlogo.png')}}" class="img-fluid" alt="logo" id="img-gone">
                        </a>
                    </div>
                    <div id="navbar-02">
                        <button id="logo-mobile" class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <button type="button" id="home-close" class="close btn-close " aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <!-- <span aria-hidden="true">&times;</span> -->
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav" style="margin-left:370px; !important">
                                <li class="nav-item ">
                                    <span class="nav-link active" id="navbarDropdownMenuLink">
                                        <a href="{{route('website-home')}}">Home
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('website-home')}}#event">Events
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('Business-List')}}">Business
                                        </a>
                                    </span>
                                </li>
                                {{-- favourite --}}
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('favourite-business')}}" >Favourites
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link" id="navbarDropdownMenuLink">
                                        <a href="{{route('website-home')}}#inquiry">
                                            Inquiry
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('website-contact')}}">Contact
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="{{route('website-logout')}}" style="color:white !important; font-size:16px;" class="btn btn-sm btn-login">Logout
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
    </header>
    <!-- header ends -->

@else
    {{-- header starts --}}
    <header>
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="navbar-nav">
                        <a class="navbar-brand smooth-scroll" href="#home">
                            <img src="{{asset('website_assets/image/mainlogo.png')}}" class="img-fluid" alt="logo" id="img-gone">
                        </a>
                    </div>
                    <div id="navbar-02">
                        <button id="logo-mobile" class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <button type="button" id="home-close" class="close btn-close " aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            <!-- <span aria-hidden="true">&times;</span> -->
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item ">
                                    <span class="nav-link active" id="navbarDropdownMenuLink">
                                        <a href="#home">Home
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="event.html">Events
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="businesslist.html">Business
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link" id="navbarDropdownMenuLink">
                                        <a href="#inquiry">
                                            Inquiry
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link " id="navbarDropdownMenuLink">
                                        <a href="contact.html">Contact
                                        </a>
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <span class="nav-link" id="navbarDropdownMenuLink">
                                        <a href="#contact" style="color:white !important; font-size:16px;" class="btn login-trigger btn-sm btn-login" data-target="#login" data-toggle="modal">Login
                                        </a>
                                    </span>
                                    <div id="login" class="modal fade" role="dialog">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content">
                                            <div class="modal-body">
                                                <button data-dismiss="modal" class="close btn-close">&times;</button>
                                                <h4>Login</h4>
                                                <form>
                                                <small class="float-left">User Id</small>
                                                <input type="email" name="username" id="email" required class="form-control" />
                                                <small class="float-left">Password</small>
                                                <input type="password" name="password" id="password" required class=" form-control"/>
                                                <input class="btn login" type="submit" id="login_user" value="Login"/><br>
                                                <input class="btn login"  value="Registration" class="btn login-trigger btn-sm btn-login" data-target="#Registration" data-toggle="modal" />
                                                </form>
        
                                                {{-- alert --}}
                                                <div class="alert display alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
        
                                                <div class="alert display alert-danger alert-dismissible fade show mb-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div id="Registration" class="modal fade" role="dialog">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content">
                                                <div class="modal-body">
                                                    <button data-dismiss="modal" class="close">&times;</button>
                                                    <h4>Registration</h4>
                                                    <form method="post" id="registration_form">
                                                    <small class="float-left">First Name</small>
                                                    <input type="text" name="firstname" required class="firstname form-control"/>
                                                    <small class="float-left">Last Name</small>
                                                    <input type="text" name="lastname" class="lastname form-control" />
                                                    <small class="float-left">Register As</small><br>
                                                    <input type="radio" name="register"  class="register" value="1"/>&nbsp;Event
                                                    <input type="radio" name="register" class="register" value="2"/>&nbsp;Business
                                                    <input type="radio" name="register" class="register" value="3">&nbsp;Visitor<br>
                                                    <small class="float-left">Password</small>
                                                    <input type="password" name="password"  required class="password1 form-control" />
                                                    <small class="float-left">Address</small>
                                                    <textarea name="address" required class="address form-control" ></textarea>
                                                    <small class="float-left">Email Id</small>
                                                    <input type="email" name="email" required class="email form-control"/>
                                                    <small class="float-left">Phone Number</small>
                                                    <input type="text" name="phone_no" required class="phone_no form-control"/>
                                                    <small class="float-left">Gender</small>
                                                    <input type="radio" name="gender"  class="gender" value="female"> Female
                                                    <input type="radio" name="gender" class="gender" value="male"> Male<br>
                                                    <small class="float-left">Date Of Birth</small>
                                                    <input type="text" name="dob" required class="dob form-control" />
                                                    <small class="float-left">pincode</small>
                                                    <input type="text" name="pincode" required class="pincode form-control" />
                                                    <input class="btn login login-trigger" type="submit" id="register"   value="Submit" data-target="#verification" data-toggle="modal" /><br>
                                                    </form>
                                                    {{-- alert --}}
                                                    <div class="alert display alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
            
                                                    <div class="alert display alert-danger alert-dismissible fade show mb-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
        
                                    {{-- email verification --}}
                                    <div id="verification" class="modal fade" role="dialog">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content">
                                            <div class="modal-body">
                                                <button data-dismiss="modal" class="close btn-close">&times;</button>
                                                <h4>Verification Code</h4>
                                                <form>
                                                <small class="float-left">Varification Code</small>
                                                <input type="text" name="username" class="form-control"/>
                                                <input class="btn login" value="verification" id="verification1" class="btn btn-sm btn-login"/>
                                                </form>
                                                 {{-- alert --}}
                                                 <div class="alert display alert-success alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
        
                                                 <div class="alert display alert-danger alert-dismissible fade show mb-0" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>
                                            </div>
                                        </div>
                                    </div>  
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
    </header>
    <!-- header ends -->
@endif

