<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sultani Makutano</title> 
    <link rel="shortcut icon" href="image/mainlogo.png">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> 
    <link href="{{asset('website_assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('website_assets/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('website_assets/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('website_assets/css/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link href ="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('website_assets/css/responsive-tabs.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('website_assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('website_assets/style.css')}}">
    <link rel="stylesheet" href="{{asset('website_assets/responsive.css')}}">
    <style>
        .alert-success {
            margin-top:15px;
            color: #02a499;
            background-color: #d9fffc;
        }

        .alert1{
            margin-top:15px;
            color: #02a499;
            background-color: #d9fffc;
        }

        .alert2{
            margin-top:15px;
            color: #ec4561;
            background-color: #fad0d7;
        }

        .alert-danger {
            margin-top:15px;
            color: #ec4561;
            background-color: #fad0d7;
        }

        .alert {
            position: relative;
            border: 0
        }

        .display{
            display: none;
        }

        .visible{
            display: visible;
        }

    </style>
</head>
<body>

    @include('website.inc.navbar')
    @yield('content')
    @include('website.inc.footer')

    <script src="{{asset('website_assets/js/jqquery.js')}}"></script>
    <script src="{{asset('website_assets/js/bootstrap.js')}}"></script>
    <script src="{{asset('website_assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('website_assets/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('website_assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('website_assets/js/fontawesome.min.js')}}"></script>
    @yield('scripts')
    <script>

        // Registration
        $(function(){
            $(document).on('click','#register',function(e){
                e.preventDefault();
                let object = {
                    fname  : $('.firstname').val(),
                    lname : $('.lastname').val(),
                    user_type : $('.register').val(),
                    password : $('.password1').val(),
                    address : $('.address').val(),
                    email : $('.email').val(),
                    phone_no : $('.phone_no').val(),
                    gender : $('.gender').val(),
                    dob : $('.dob').val(),
                    pincode : $('.pincode').val(),
                    _token: '{{csrf_token()}}'
                }
                //console.log(object);
                $.ajax(
                {
                    url:"http://34.202.173.112/smartevent/userregistration",
                    type:"POST",
                    data : object,
                    dataType:"json",
                    success: function(text){
                        let response = text;
                        console.log(response);
                        if(response.status == 1)
                        {
                            $('.alert-success').removeClass('display');
                            $('.alert-success').addClass('visible');
                            $('.message').html(response.msg);

                            let object = {
                                user_id : response.user_id
                            }
                            console.log(object);
                            $(document).on('click','#verification1',function(e){
                                e.preventDefault();
                                console.log('hii');
                                $.ajax({
                                    url:"http://34.202.173.112/smartevent/api/verification",
                                    type:"POST",
                                    data : object,
                                    dataType:"json",
                                    success: function(text)
                                    {
                                        let response = text;
                                        if(response.status == 1)
                                        {
                                            $('.verification_text').removeClass('display');
                                            $('.verification_text').addClass('visible');
                                            $('.message1').html(response.msg);
                                            window.location.href = "{{route('website-home')}}";
                                        }
                                        else
                                        {
                                            $('.verification_text1').removeClass('display');
                                            $('.verification_text1').addClass('visible');
                                            $('.message1').html(response.msg);
                                        }
                                    }
                                });
                            });
                        }
                        else
                        {
                            $('.alert-danger').removeClass('display');
                            $('.alert-danger').addClass('visible');
                        }
                    },
                });
                $('.fname').val('');
                $('.lname').val('');
                $('.email').val('');
                $('.phone_no').val('');
                $('.user_type').val('');
                $('.address').val('');
                $('.register').val('');
                $('.gender').val('');
                $('.dob').val('');
                $('.pincode').val('');
            });
        });

        // login
        $(function(){
            $(document).on('click','#login_user',function(e){
                e.preventDefault();
                let object = {
                    email : $('#email').val(),
                    password : $('#password').val(),
                    _token : '{{csrf_token()}}'
                }
                console.log(object);

                $.ajax({
                    url : "http://34.202.173.112/smartevent/website/login",
                    type : "POST",
                    data : object,
                    dataType:"json",
                    success : function(text)
                    {
                        let response = text;
                        console.log(response);

                        if(response.status == 1)
                        {
                            let response1 = response.data;
                            console.log(response1.user_id);
                            $('.alert-success').removeClass('display');
                            $('.alert-success').addClass('visible');
                            $('.message').html(response.msg);
                            window.location.href = "{{route('website-home')}}";
                        }
                        else
                        {
                            $('.alert-danger').removeClass('display');
                            $('.alert-danger').addClass('visible');
                            $('.message').html(response.msg);
                        }
                    }
                });
            });
        });

    </script>
    <script src="{{asset('website_assets/app.js')}}"></script>
</body>
</html>


