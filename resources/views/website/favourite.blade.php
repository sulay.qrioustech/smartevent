@extends('website.inc.app')
@section('content')
     <!-- preloader -->
    <section id="preloader">
        <div id="pre">  
            &nbsp;
        </div>
    </section>
    <!-- preloader end -->

    <!-- content and image section -->
    <section id="home" class="owl-carousel owl-theme">
        <div class="card mb-3">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image5.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image6.webp')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image3.jpg')}}" alt="Card image cap">
          </div>
          <div class="card mb-3 ">
            <img class="card-img-top owl" src="{{asset('website_assets/image/home_image1')}}" alt="Card image cap">
          </div>
    </section>

     {{-- alert --}}
     <div class="alert  display alert-success alert-dismissible fade show" style="margin-top:50px;" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> <strong class="message"></strong></div>
        
     <div class="alert display alert-danger alert-dismissible fade show mb-0" style="margin-top:50px;"  role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong class="message"></strong></div>

    <!-- upcomming and past event -->
    <section id="event">
        <div class="content-box-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-12">
                        <div id="event_left">
                            <div class="poster_image">
                                <img src="image/advertisement3.jpg" class="img-fluid imagespostar" alt="image">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div id="event_right row">
                            <!-- upcomming -->
                            <div class="event_heading col-md-12">
                                <h4 class="text-left"  style=" margin-top:1
                                0px; color:#cf1718 !important;">Favourite Business</h4>
                            </div>
                            <div id="upcommingbusiness" class="upcommingevents owl-carousel owl-theme">
                                {{-- <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business1.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Qatello</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business2.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Voroly</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business3.jpg" alt="images"/></a> 
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Monicy</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/business4.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Rumpes</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div>
                                <div class="col-md-4 demo owl">
                                    <div class="event_image">
                                        <a href="singleBusiness.html"><img src="image/gameover.jpg" alt="images"/></a>
                                    </div>
                                    <div class="image_description">
                                        <div class="favourites "><i class="fa fa-heart" style="color:#CD2129;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px;">50</span></i></div>
                                        <div class="heading" style="margin-top:10px;"><p>Nultat</p></div>
                                        <div>&nbsp;</div>
                                    </div> 
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcomming and past event ends -->
@endsection

@section('scripts')
    <script>
        let user_id ="{{session('user_id')}}"

        $(function(){

            let object = {
                user_id : "{{session('user_id')}}",
                _token : "{{csrf_token()}}"
            }
            console.log(object)

            $.ajax({
                url: "http://34.202.173.112/smartevent/favourite/list",
                type: "POST",
                data: object,
                dataType: "json",
                success : function(data){
                    let response = data.data;
                    console.log(response);
                    $('#upcommingbusiness').html('');
                    $.each(response,function(index,item){
                        $('#upcommingbusiness').append(`
                            <div class="col-md-4 demo owl">
                                <div class="event_image">
                                    <a href="http://34.202.173.112/smartevent/business/${item.business.business_id}"><img src="${item.business.logo_image}" alt="images"/></a>
                                </div>
                                <div class="image_description">
                                    <div class="favourites">
                                    <a class="btn" data-id="${item.favourite_id}" id="Like"><i class="fa heart fa-heart" style="color:#CD2129; margin-left:-40px;" aria-hidden="true">&nbsp;<span style="color: #555555; font-size:16px; "></span></i></div>
                                    <div class="heading" style="margin-top:10px; margin-left:-80px;"><p>${item.business.company_name}</p></div>
                                    <div>&nbsp;</div>
                                </div> 
                            </div>
                        `);
                    });
                }
            });
        });
        
        $(function(){
            $(document).on('click','#Like',function(e){
                e.preventDefault();

                let id1 = $('#Like').attr("data-id");
                console.log(id1);

                let object = {
                    favourite_id : id1,
                    _token : "{{csrf_token()}}"
                }
                console.log(object)
                $.ajax({
                    url : "http://34.202.173.112/smartevent/favourite/delete",
                    type : "post",
                    dataType : "json",
                    data : object,
                    success : function(data)
                    {
                        let response = data;
                        console.log(response);

                        // $(".heart").css({"color": "gray","border-color":"black"});
                        
                        
                    }
                });
            });
        });
    </script>
@endsection