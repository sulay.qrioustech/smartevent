@component('mail::message')
Thank You For Contact Us.{{$data['name']}}..We Will Contact Soon.

{{-- 
@component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
