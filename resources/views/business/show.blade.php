@extends('layouts.app')
@section('content')
    <div class="box-typical box-typical-padding">
        <div class="box-typical-body">
            <div class="table-responsive">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2 style="font-weight:510; font-size:28px; margin-left:10px;">Business</h2>
                                {{-- <div class="subtitle">Welcome to Ultimate Dashboard</div> --}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="card">
                    <div class="card-block">
                        <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>logo</th>
                                <th>Business Organizar Detail
                                <th>Favourites User</th>
                                <th>Approve Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($business as $value)
                                <tr>
                                    <td>{{$loop->index+1}}  
                                    <td>{{$value->company_name}}
                                    <td><img src="{{asset('storage/images/'.$value->logo_image)}}" class="img-fluid" style="height: 60px; width:60px;" alt="image">
                                    <td>
                                        <a href ="{{route('business-organizar-detail',['id' => $value->user_id])}}">
                                            <span class="label label-success">Business Organizar Detail
                                            </span>
                                        </a>
                                    <td>@php
                                            $favourite = App\Favourite::where('business_id',$value->business_id)->count();
                                        @endphp
                                        <a href="{{route('favourite-user',['id' => $value->business_id])}}">
                                        <span class="label label-primary">{{$favourite}}</span></a>
                                    </td>
                                    <td>
                                        @if($value->business_status == 0)
                                            <a class="btn btn-sm btn-danger"  id="event_status" data-toggle="modal" data-target="#exampleModal{{$value->business_id}}" >Pending</a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal{{$value->business_id}}" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are You Sure Want to Approve?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-sm btn-secondary">Cancle</a>
                                                        <a class="btn btn-sm btn-danger"  href="{{route('business-approve',['id' => $value->business_id])}}">Approve</a>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        @else
                                            <a class="btn btn-sm btn-primary" href="{{route('business-disapprove',['id' =>
                                                $value->business_id])}}" data-toggle="modal" data-target="#exampleModall{{$value->business_id}}" >Approve</a>   


                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModall{{$value->business_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are You Sure Want to Dis-Approve?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-sm btn-secondary">Cancle</a>
                                                        <a class="btn btn-sm btn-danger" href="{{route('business-disapprove',['id' => $value->business_id])}}">DisApprove</a>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>  
@endsection